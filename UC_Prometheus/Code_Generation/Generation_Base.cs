﻿///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
///////////////////////////////////////////////////////
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UC_Prometheus.Models;

namespace UC_Prometheus.Code_Generation
{
    public class Generation_Base
    {

        #region Private Variables

        protected CodeTypeDeclaration targetClass;
        protected CodeCompileUnit targetUnit;
        protected string outPutFileName = "";

        #endregion

        /// <summary>
        /// The Constructor for the Base Class
        /// </summary>
        protected Generation_Base()
        {

        }

        /// <summary>
        /// Initializes the Class
        /// </summary>
        protected virtual void CreateClass(string name, string baseType, FrameWork frameWork)
        {
            #region Initial values Construction

            outPutFileName = name + ".cs";
            targetUnit = new CodeCompileUnit();
            CodeNamespace nameSpace = new CodeNamespace(DataBank.ProjectName);
            targetClass = new CodeTypeDeclaration(name);

            #endregion

            AddNameSpaces(nameSpace, frameWork);

            TargetClassInitiation(nameSpace, baseType);

        }

        private void TargetClassInitiation(CodeNamespace nameSpace,string baseType)
        {
            targetClass.IsClass = true;
            if (baseType != null)
            {
                targetClass.BaseTypes.Add(new CodeTypeReference
                { BaseType = baseType, Options = CodeTypeReferenceOptions.GenericTypeParameter });
            }

            targetClass.TypeAttributes = System.Reflection.TypeAttributes.Public;
            nameSpace.Types.Add(targetClass);
            targetUnit.Namespaces.Add(nameSpace);
        }

        /// <summary>
        /// Assigns the namespaces for the class to be produced
        /// </summary>
        /// <param name="nameSpace">The Code Namespace to be assign</param>
        protected void AddNameSpaces(CodeNamespace nameSpace,FrameWork frameWork)
        {
            switch(frameWork)
            {
                case FrameWork.Monogame:
                    ImportMonogameNamespaces(nameSpace);
                    break;
                case FrameWork.XNA:
                    ImportXNANamespaces(nameSpace);
                    break;
            }
        }

        /// <summary>
        /// Imports the XNA namespaces
        /// </summary>
        /// <param name="nameSpace">The variable in which the namespaces are imported</param>
        private void ImportXNANamespaces(CodeNamespace nameSpace)
        {
            try
            {
                #region Namespace Assignment

                nameSpace.Imports.Add(new CodeNamespaceImport("System"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.IO"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.Linq"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Audio"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Content"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.GamerServices"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Graphics"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Input"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Media"));
                nameSpace.Imports.Add(new CodeNamespaceImport("GCG_Framework"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.Reflection"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.Xml.Serialization"));

                #endregion
            }
            catch(Exception e)
            {
                Console.WriteLine("The following Error has occured while trying to Import the XNA namespaces:\n"
                    + e.StackTrace);
            }
        }

        /// <summary>
        /// Imports the Monogame Namespaces
        /// </summary>
        /// <param name="nameSpace">The variable for the Namespaces to be added</param>
        private void ImportMonogameNamespaces(CodeNamespace nameSpace)
        {
            try
            {

                #region Namespace assignent

                nameSpace.Imports.Add(new CodeNamespaceImport("System"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.IO"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.Linq"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Audio"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Content"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.GamerServices"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Graphics"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Input"));
                nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Media"));
                nameSpace.Imports.Add(new CodeNamespaceImport("GCG_Framework"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.Reflection"));
                nameSpace.Imports.Add(new CodeNamespaceImport("System.Xml.Serialization"));

                #endregion

            }
            catch(Exception e)
            {
                Console.WriteLine("The following Error has occured while trying to Import the Monogame namespaces:\n"
                    + e.StackTrace);
            }
        }

        /// <summary>
        /// Adds the class to a specific directory in a C# file
        /// </summary>
        protected virtual void CookClass()
        {
            try
            {
                CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");    //Initiate the Provider with C# as language
                CodeGeneratorOptions options = new CodeGeneratorOptions(); //Initiate the Options
                options.BracingStyle = "C"; //Assign syntax Style
                using (StreamWriter sourceWriter = new StreamWriter(DataBank.GameDirectory + "\\" + outPutFileName)) //Call StreamReader to Write Class
                {
                    provider.GenerateCodeFromCompileUnit(
                        targetUnit, sourceWriter, options); //Write the Class Based on Previous Variabes
                }
            }
            catch (Exception ex)    //In any case of Exception (Administrator previlages etc.)
            {
                Console.WriteLine("The Following Error Has Occured While Trying to Write Class " + outPutFileName + "\n");
                Console.WriteLine(ex.StackTrace);

            }
        }

    }
}
