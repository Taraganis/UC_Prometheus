﻿///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
///////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UC_Prometheus.Code_Generation
{
    public class GameStream
    {

        #region Class Variables 

        private List<string> filePaths;
        private string filePath;
        private string nameSpace;
        private List<string> outputFile;
        private string className;
        private List<string> classNames;

        #endregion

        /// <summary>
        /// Generates a class based on the nameSpace based
        /// </summary>
        /// <param name="nameSpace">The namespace for the class</param>
        /// <param name="filePath">the filepath of the original class</param>
        /// <param name="className">the class name to be selected</param>
        public GameStream(string nameSpace,string filePath,string className)
        {

            this.nameSpace = nameSpace;
            this.filePath = filePath;
            this.className = className;

        }

        /// <summary>
        /// Generates selection of classes based on the nameSpace based
        /// </summary>
        /// <param name="nameSpace">The namespace for the classes</param>
        /// <param name="filePaths">the filepaths of the original classes</param>
        /// <param name="classNames">the class names to be selected</param>
        public GameStream(string nameSpace,List<string> filePaths,List<string> classNames)
        {
            this.nameSpace = nameSpace;
            this.filePaths = filePaths;
            this.classNames = classNames;
        }
        /// <summary>
        /// Generates a class
        /// </summary>
        public void GenerateClass()
        {
            try
            {
                LineCheck(filePath);
                AppendNewClass(className);
            }
            catch(Exception e)
            {
                Console.WriteLine("The following Error has Occured :" + e.StackTrace);
            }

        }
        /// <summary>
        /// Generates Multiple classes
        /// </summary>
        public void GenerateClasses()
        {
            try
            {
                for (int i = 0; i < filePaths.Count; i++)
                {
                    LineCheck(filePaths[i]);
                    AppendNewClass(classNames[i]);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("The Following Error Has Occured:" + e.StackTrace);
            }
        }

        /// <summary>
        /// Sets the nameSpace (The original is UCDemoByHand)
        /// </summary>
        /// <param name="filePath"></param>
        private void LineCheck(string filePath)
        {
            outputFile = new List<string>();
            using (StreamReader streamReader = new StreamReader(filePath))
            {
                while(!streamReader.EndOfStream)
                {
                    string line = streamReader.ReadLine().TrimStart();
                    if (line == "namespace UCDemoByHand")   //Checks for the tag
                    {
                        line = "namespace " + nameSpace;
                    }
                    CheckResolution(ref line);

                    outputFile.Add(line);
                }
            }


        }

        /// <summary>
        /// Checks and Sets the Resolution of the Game
        /// </summary>
        /// <param name="line"></param>
        private void CheckResolution(ref string lineRef)
        {
            string line = lineRef.TrimStart().TrimEnd();

            if (line == "graphics.PreferredBackBufferHeight = 600;")    //Setting Height
            {
                line = "graphics.PreferredBackBufferHeight = " + DataBank.fullGame.ScreenHeight.ToString() + ";";
                lineRef = line;
            }
            if (line == "graphics.PreferredBackBufferWidth = 800;") //Setting Width
            {
                line = "graphics.PreferredBackBufferWidth = " + DataBank.fullGame.ScreenWidth.ToString() + ";";
                lineRef = line;
            }


        }
        
        /// <summary>
        /// Appends new Class
        /// </summary>
        private void AppendNewClass(string className)
        {
            using (StreamWriter streamWriter = new StreamWriter(DataBank.GameDirectory 
                + "\\" + className))
            {
                foreach (string line in outputFile)
                    streamWriter.WriteLine(line);
            }
           

        }
    }
}
