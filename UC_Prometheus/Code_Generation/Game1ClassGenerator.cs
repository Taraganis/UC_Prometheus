﻿///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
///////////////////////////////////////////////////////
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UC_Prometheus;
using UC_Prometheus.Models;

namespace UC_Prometheus.Code_Generation
{

    
    public static class Game1Class_Generator
    {

        #region Private Static Varibles

        private static CodeTypeDeclaration targetClass;
        private static CodeCompileUnit targetUnit;
        private static string outPutFileName = "";

        #endregion

        public static void CreateClass(FullGame game)
        {
            outPutFileName = "Game1";
            targetUnit = new CodeCompileUnit();
            CodeNamespace nameSpace = new CodeNamespace(DataBank.ProjectName);
            targetClass = new CodeTypeDeclaration("Game1");

            //Adding NameSpaces to the 
            //class according to the Framework Selected

            if (game.framework == FrameWork.XNA)
                AddNameSpacesXNA(nameSpace);
            else
                AddNameSpacesMono(nameSpace);

            #region Target Class Initiation

            targetClass.IsClass = true;
            targetClass.BaseTypes.Add(new CodeTypeReference
            { BaseType = "Microsoft.Xna.Framework.Game", Options = CodeTypeReferenceOptions.GenericTypeParameter });
            targetClass.TypeAttributes = System.Reflection.TypeAttributes.Public;
            nameSpace.Types.Add(targetClass);
            targetUnit.Namespaces.Add(nameSpace);

            #endregion

            AddFields(game);

            AddConstructor(game); //Adds the new Values to the constructor

            CookClass(); //Cooks the class and exports it to the destination specified
        }

        /// <summary>
        /// Adds Fields to the output Class (Really Long Method. I warn you xD)
        /// </summary>
        private static void AddFields(FullGame game)
        {
          

            CodeMemberField graphics = new CodeMemberField();
            graphics.Attributes = MemberAttributes.Public;
            graphics.Name = "graphics";
            graphics.Type = new CodeTypeReference("GraphicsDeviceManager", CodeTypeReferenceOptions.GenericTypeParameter);
            targetClass.Members.Add(graphics);

            CodeMemberField spriteBatch = new CodeMemberField();
            spriteBatch.Attributes = MemberAttributes.Public;
            spriteBatch.Name = "spriteBatch";
            spriteBatch.Type = new CodeTypeReference("SpriteBatch", CodeTypeReferenceOptions.GenericTypeParameter);
            targetClass.Members.Add(spriteBatch);

            CodeMemberField cheatCode = new CodeMemberField();
            cheatCode.Attributes = MemberAttributes.Public;
            cheatCode.Name = "cheatCode";
            cheatCode.Type = new CodeTypeReference(typeof(string));
            cheatCode.InitExpression = new CodeSnippetExpression("1sx6");
            targetClass.Members.Add(cheatCode);

         

        }   //Adding the fields to the Game1 class

        /// <summary>
        /// Adds the Constructor to the new Class to be generated
        /// </summary>
        private static void AddConstructor(FullGame game)
        {
            CodeConstructor codeConstructor = new CodeConstructor();
            codeConstructor.Attributes = MemberAttributes.Public | MemberAttributes.Final;

            #region Adding all things to the Constructor

            CodeFieldReferenceExpression graphics =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "graphics");
            codeConstructor.Statements.Add(new CodeAssignStatement(graphics,
                new CodeSnippetExpression("new GraphicsDeviceManager(this)")));

            CodeFieldReferenceExpression Content =
               new CodeFieldReferenceExpression(
               new CodeFieldReferenceExpression(), "Content.RootDirectory");
            codeConstructor.Statements.Add(new CodeAssignStatement(graphics,
                new CodePrimitiveExpression("Content")));

            CodeFieldReferenceExpression PreferredBackBufferHeight =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "graphics.PreferredBackBufferHeight");
            codeConstructor.Statements.Add(new CodeAssignStatement(PreferredBackBufferHeight,
                new CodeSnippetExpression(game.ScreenHeight.ToString())));

            CodeFieldReferenceExpression PreferredBackBufferWidth =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "graphics.PreferredBackBufferWidth");
            codeConstructor.Statements.Add(new CodeAssignStatement(PreferredBackBufferWidth,
                new CodeSnippetExpression(game.ScreenWidth.ToString())));

            #endregion

            targetClass.Members.Add(codeConstructor); //Adding The constructor to the target Class
        }

        #region NameSpace Initiation

        /// <summary>
        /// Adds the nameSpaces into the class
        /// </summary>
        /// <param name="nameSpace">The CodeNameSpace for importing</param>
        private static void AddNameSpacesXNA(CodeNamespace nameSpace)
        {
            nameSpace.Imports.Add(new CodeNamespaceImport("System"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.IO"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.Linq"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Audio"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Content"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.GamerServices"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Graphics"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Input"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Media"));
            nameSpace.Imports.Add(new CodeNamespaceImport("GCG_Framework"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.Reflection"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.Xml.Serialization"));
            nameSpace.Imports.Add(new CodeNamespaceImport("UCDemoByHand"));
        }

        /// <summary>
        /// Adds the namespaces into the class along with the mono namespaces
        /// </summary>
        /// <param name="nameSpace"></param>
        private static void AddNameSpacesMono(CodeNamespace nameSpace)
        {
            nameSpace.Imports.Add(new CodeNamespaceImport("System"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.IO"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.Collections.Generic"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.Linq"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Audio"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Content"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.GamerServices"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Graphics"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Input"));
            nameSpace.Imports.Add(new CodeNamespaceImport("Microsoft.Xna.Framework.Media"));
            nameSpace.Imports.Add(new CodeNamespaceImport("GCG_Framework"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.Reflection"));
            nameSpace.Imports.Add(new CodeNamespaceImport("System.Xml.Serialization"));
            nameSpace.Imports.Add(new CodeNamespaceImport("UCDemoByHand"));
        }

        #endregion

        #region Class Internal Methods

        /// <summary>
        /// Adds the Init Method to the class
        /// </summary>
        private static void AddInitMethod(FullGame game)
        {
            // Declaring a ToString method
            CodeMemberMethod init = new CodeMemberMethod();
            init.Attributes =
                MemberAttributes.Public;
            init.Name = "init";
            init.ReturnType =
                new CodeTypeReference();

            CodeFieldReferenceExpression border =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "border");
            init.Statements.Add(new CodeAssignStatement(border,
                 new CodeSnippetExpression("40")));

            CodeSnippetExpression screenWidth =
                new CodeSnippetExpression("G.ResolutionWidth = " + game.ScreenWidth.ToString());
            init.Statements.Add(screenWidth);

            CodeSnippetExpression screenHeight =
                new CodeSnippetExpression("G.ResolutionHeight = " + game.ScreenHeight.ToString());
            init.Statements.Add(screenHeight);

            CodeFieldReferenceExpression tempResourceDir =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "tempResourceDir");
            init.Statements.Add(new CodeAssignStatement(tempResourceDir,
                 new CodeSnippetExpression("Directory.GetCurrentDirectory() + resources")));

            CodeFieldReferenceExpression resourceDir =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "resourceDir");
            init.Statements.Add(new CodeAssignStatement(resourceDir,
                 new CodeSnippetExpression("Directory.GetCurrentDirectory() + resources")));



            //CodeFieldReferenceExpression resourceDir =
            //    new CodeFieldReferenceExpression(
            //    new CodeThisReferenceExpression(), "border");
            //init.Statements.Add(new CodeAssignStatement(border,
            //     new CodeSnippetExpression("Directory.GetCurrentDirectory() + "\\Resources\\"")));

            #region Fonts

            CodeFieldReferenceExpression game_SmallFont =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "game_SmallFont");
            init.Statements.Add(new CodeAssignStatement(game_SmallFont,
                 new CodeSnippetExpression("G.fnt_courier12")));

            CodeFieldReferenceExpression game_MediumFont =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "game_MediumFont");
            init.Statements.Add(new CodeAssignStatement(game_MediumFont,
                 new CodeSnippetExpression("G.fnt_courier14")));

            CodeFieldReferenceExpression game_BigFont =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "game_BigFont");
            init.Statements.Add(new CodeAssignStatement(game_BigFont,
                 new CodeSnippetExpression("G.fnt_courier26")));

            #endregion

            #region Array Initiation

            CodeFieldReferenceExpression lev =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "lev");
            init.Statements.Add(new CodeAssignStatement(lev,
                 new CodeSnippetExpression("new PLevel[maxNumOfPLevels]")));

            CodeFieldReferenceExpression creeps =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creeps");
            init.Statements.Add(new CodeAssignStatement(creeps,
                 new CodeSnippetExpression("new PCreepFactory[maxNumOfCreeps]")));

            CodeFieldReferenceExpression bullets =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bullets");
            init.Statements.Add(new CodeAssignStatement(bullets,
                 new CodeSnippetExpression("new PBullet[maxNumOfBullets]")));

            #endregion

            #region Screen Initiation

            CodeFieldReferenceExpression intro =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "intro ");
            init.Statements.Add(new CodeAssignStatement(intro,
                 new CodeSnippetExpression("new Intro()")));

            CodeFieldReferenceExpression win =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "win");
            init.Statements.Add(new CodeAssignStatement(win,
                 new CodeSnippetExpression("new Win()")));

            CodeFieldReferenceExpression fail =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "fail");
            init.Statements.Add(new CodeAssignStatement(fail,
                 new CodeSnippetExpression("new Fail()")));

            CodeFieldReferenceExpression menu =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "menu");
            init.Statements.Add(new CodeAssignStatement(menu,
                 new CodeSnippetExpression("new Menu()")));

            CodeFieldReferenceExpression about =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "about");
            init.Statements.Add(new CodeAssignStatement(about,
                 new CodeSnippetExpression("new Menu()")));

            CodeFieldReferenceExpression help1 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "help1");
            init.Statements.Add(new CodeAssignStatement(help1,
                 new CodeSnippetExpression("new Menu()")));

            CodeFieldReferenceExpression help2 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "help2");
            init.Statements.Add(new CodeAssignStatement(help2,
                 new CodeSnippetExpression("new Menu()")));

            CodeFieldReferenceExpression help3 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "help3");
            init.Statements.Add(new CodeAssignStatement(help3,
                 new CodeSnippetExpression("new Menu()")));


            #endregion

            #region Calling Loaders

            CodeSnippetExpression LoadBullets =
                new CodeSnippetExpression("LoadBullets()");
            init.Statements.Add(LoadBullets);

            CodeSnippetExpression LoadCreeps =
                new CodeSnippetExpression("LoadCreeps()");
            init.Statements.Add(LoadCreeps);

            CodeSnippetExpression LoadLevels =
                new CodeSnippetExpression("LoadLevels()");
            init.Statements.Add(LoadLevels);

            #endregion



            targetClass.Members.Add(init);

        }

        /// <summary>
        /// Creates the UnloadContent Method of the Class
        /// </summary>
        private static void AddUnloadMethod()
        {
            CodeMemberMethod unload = new CodeMemberMethod();
            unload.Attributes =
                MemberAttributes.Family | MemberAttributes.Override;
            unload.Name = "UnloadContent";
            unload.ReturnType =
                new CodeTypeReference();

            CodeCommentStatement comment = new CodeCommentStatement(
                new CodeComment("TODO: Unload any non ContentManager content here"));

            unload.Statements.Add(comment);

            targetClass.Members.Add(unload);
        }

        /// <summary>
        /// Adds Initialize Method to the Game1 class
        /// </summary>
        private static void Initialize()
        {
            CodeMemberMethod initialize = new CodeMemberMethod();
            initialize.Attributes =
                MemberAttributes.Family | MemberAttributes.Override;
            initialize.Name = "Initialize";
            initialize.ReturnType =
                new CodeTypeReference();

            CodeCommentStatement comment = new CodeCommentStatement(
                new CodeComment("TODO: Add your initialization logic here"));
            initialize.Statements.Add(comment);


            CodeSnippetExpression InitializeBase =
             new CodeSnippetExpression("base.Initialize()");
            initialize.Statements.Add(InitializeBase);

            initialize.Statements.Add(InitializeBase);

            targetClass.Members.Add(initialize);

        }

        /// <summary>
        /// Adds Load Content Method to the Game1 Class
        /// </summary>
        private static void LoadContent()
        {
            CodeMemberMethod loadContent = new CodeMemberMethod();
            loadContent.Attributes =
                MemberAttributes.Family | MemberAttributes.Override;
            loadContent.Name = "Initialize";
            loadContent.ReturnType =
                new CodeTypeReference();

            #region Statements

            CodeFieldReferenceExpression spriteBatch =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "spriteBatch");
            loadContent.Statements.Add(new CodeAssignStatement(spriteBatch,
                 new CodeSnippetExpression("new SpriteBatch(GraphicsDevice)")));

            CodeSnippetExpression screenWidth =
                new CodeSnippetExpression("G.screenWidth = GraphicsDevice.Viewport.Width");
            loadContent.Statements.Add(screenWidth);

            CodeSnippetExpression screenHeight =
                new CodeSnippetExpression("G.ScreenHeight = GraphicsDevice.Viewport.Height");
            loadContent.Statements.Add(screenHeight);

            CodeSnippetExpression screenRectangle =
               new CodeSnippetExpression("G.screenRect = new Rectangle(0, 0, G.screenWidth, G.ScreenHeight)");
            loadContent.Statements.Add(screenRectangle);

            CodeSnippetExpression lineBatchInit =
               new CodeSnippetExpression("LineBatch.init(GraphicsDevice)");
            loadContent.Statements.Add(lineBatchInit);

            #region Initiate Fonts

            CodeSnippetExpression courier12 =
               new CodeSnippetExpression("G.fnt_courier12 = Content.Load<SpriteFont>(\"Font0\")");
            loadContent.Statements.Add(courier12);

            CodeSnippetExpression courier14 =
               new CodeSnippetExpression("G.fnt_courier14 = Content.Load<SpriteFont>(\"Font1\")");
            loadContent.Statements.Add(courier14);

            CodeSnippetExpression courier26 =
               new CodeSnippetExpression("G.fnt_courier26 = Content.Load<SpriteFont>(\"Font2\")");
            loadContent.Statements.Add(courier26);

            CodeSnippetExpression times14 =
               new CodeSnippetExpression("G.fnt_times14 = Content.Load<SpriteFont>(\"Font3\")");
            loadContent.Statements.Add(times14);

            CodeSnippetExpression times20 =
               new CodeSnippetExpression("G.fnt_times20 = Content.Load<SpriteFont>(\"Font4\")");
            loadContent.Statements.Add(times20);

            CodeSnippetExpression motorwerk14 =
               new CodeSnippetExpression("G.fnt_Motorwerk14 = Content.Load<SpriteFont>(\"Font5\")");
            loadContent.Statements.Add(motorwerk14);

            CodeSnippetExpression motorwerk26 =
               new CodeSnippetExpression("G.fnt_Motorwerk26 = Content.Load<SpriteFont>(\"Font6\")");
            loadContent.Statements.Add(motorwerk26);

            CodeSnippetExpression quartz_ms13 =
               new CodeSnippetExpression("G.fnt_Quartz_ms13 = Content.Load<SpriteFont>(\"Font7\")");
            loadContent.Statements.Add(quartz_ms13);

            CodeSnippetExpression quartz_ms22 =
               new CodeSnippetExpression("G.fnt_Quartz_ms22 = Content.Load<SpriteFont>(\"Font8\")");
            loadContent.Statements.Add(quartz_ms22);

            #endregion

            #region Game Parameter Class

            CodeSnippetExpression gpp =
               new CodeSnippetExpression("GameParamsClass gpp = new GameParamsClass()");
            loadContent.Statements.Add(gpp);

            CodeSnippetExpression gppInit =
               new CodeSnippetExpression("gpp.init()");
            loadContent.Statements.Add(gppInit);

            #endregion

            CodeSnippetExpression initGlobals =
              new CodeSnippetExpression("G.initGlobals(gpp, GraphicsDevice)");
            loadContent.Statements.Add(initGlobals);

            #region Initiate And Load Sounds

            CodeSnippetExpression booms =
              new CodeSnippetExpression("G.booms = new GCG_Booms(GraphicsDevice)");
            loadContent.Statements.Add(booms);

            CodeSnippetExpression snd_boom =
              new CodeSnippetExpression("G.snd_boom = new LimitSound(\"BoomSound2\", 3, Content)");
            loadContent.Statements.Add(snd_boom);

            #endregion

            #region Level Manager Needs a loop cause I am bored

            CodeSnippetExpression initLevelManager =
              new CodeSnippetExpression("G.levelManager = new LevelManager()");
            loadContent.Statements.Add(initLevelManager);

            CodeSnippetExpression initLevel0 =
              new CodeSnippetExpression("G.level0 = new Level0()");
            loadContent.Statements.Add(initLevelManager);


            #endregion


            CodeSnippetExpression setFirstLevel =
              new CodeSnippetExpression("G.levelManager.setLevel(0)");
            loadContent.Statements.Add(setFirstLevel);



            #endregion

            targetClass.Members.Add(loadContent);
        }

        /// <summary>
        /// Adds the Load Level Method to the Class
        /// </summary>
        private static void AddLoadLevels(FullGame game)
        {
            CodeMemberMethod loadLevels = new CodeMemberMethod();
            loadLevels.Attributes =
                MemberAttributes.Private;
            loadLevels.Name = "LoadLevels";
            loadLevels.ReturnType =
                new CodeTypeReference();

            for (int i = 0; i < game.levels.Count(); i++)
            {
                CodeFieldReferenceExpression level_Initiation =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "lev[" + i.ToString() + "]");
                loadLevels.Statements.Add(new CodeAssignStatement(level_Initiation,
                     new CodeSnippetExpression("new " + game.levels[i].Name + "()")));
            }


            #endregion

            targetClass.Members.Add(loadLevels);
        }

        /// <summary>
        /// Adds the Load Creeps method to the class
        /// </summary>
        private static void AddLoadCreeps(FullGame game)
        {
            CodeMemberMethod loadCreeps = new CodeMemberMethod();
            loadCreeps.Attributes =
                MemberAttributes.Private;
            loadCreeps.Name = "LoadCreeps";
            loadCreeps.ReturnType =
                new CodeTypeReference();

            #region Creeps

            for (int i = 0; i < game.creeps.Count(); i++)
            {
                if (game.creeps.Count() > 1)
                {
                    CodeFieldReferenceExpression creep_Initiation =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creeps[" + i.ToString() + "]");
                    loadCreeps.Statements.Add(new CodeAssignStatement(creep_Initiation,
                         new CodeSnippetExpression("new " + game.creeps[i].Name + "()")));
                }
                else if (game.creeps.Count() == 1)
                {
                    CodeFieldReferenceExpression creep_Initiation =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creeps[" + i + 1.ToString() + "]");
                    loadCreeps.Statements.Add(new CodeAssignStatement(creep_Initiation,
                         new CodeSnippetExpression("new " + game.creeps[i].Name + "()")));
                    CodeFieldReferenceExpression creep_Initiation2 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creeps[" + i.ToString() + "]");
                    loadCreeps.Statements.Add(new CodeAssignStatement(creep_Initiation2,
                         new CodeSnippetExpression("new " + game.creeps[i].Name + "()")));
                }
            }


            #endregion



            targetClass.Members.Add(loadCreeps);
        }

        /// <summary>
        /// Adds the load bullets method to the class
        /// </summary>
        private static void AddLoadBullets(FullGame game)
        {
            CodeMemberMethod loadBullets = new CodeMemberMethod();
            loadBullets.Attributes =
                MemberAttributes.Private;
            loadBullets.Name = "LoadBullets";
            loadBullets.ReturnType =
                new CodeTypeReference();

            for (int i = 0; i < game.bullets.Count; i++)
            {
                CodeFieldReferenceExpression bullet_Initiation =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "bullets[" + i.ToString() + "]");
                loadBullets.Statements.Add(new CodeAssignStatement(bullet_Initiation,
                     new CodeSnippetExpression("new " + game.bullets[i].Name + "()")));
            }

            targetClass.Members.Add(loadBullets);
        }





        /// <summary>
        /// Cooks and exports the Class
        /// </summary>
        private static void CookClass()
        {
            try
            {
                CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
                CodeGeneratorOptions options = new CodeGeneratorOptions();
                options.BracingStyle = "C";
                using (StreamWriter sourceWriter = new StreamWriter(@"Output\" + DataBank.GameName + "\\" + outPutFileName + ".cs"))
                {
                    provider.GenerateCodeFromCompileUnit(
                        targetUnit, sourceWriter, options);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Missing Information or other problem");
                Console.WriteLine(e.StackTrace);
            }
        }

        /// <summary>
        /// Cooks and exports the Class based on a full game model
        /// </summary>
        private static void CookClass(FullGame game)
        {
            try
            {
                CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
                CodeGeneratorOptions options = new CodeGeneratorOptions();
                options.BracingStyle = "C";
                using (StreamWriter sourceWriter = new StreamWriter(DataBank.GameDirectory + "\\" + outPutFileName + ".cs"))
                {
                    provider.GenerateCodeFromCompileUnit(
                        targetUnit, sourceWriter, options);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Missing Information or other problem");
                Console.WriteLine(e.StackTrace);
            }
        }


    }
}
