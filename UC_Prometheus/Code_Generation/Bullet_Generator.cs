﻿///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
///////////////////////////////////////////////////////

using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UC_Prometheus;
using UC_Prometheus.Models;
using UC_Prometheus.Code_Generation;

namespace UC_Prometheus.Code_Generation
{
    public class Bullet_Generator : Generation_Base
    {

       
        /// <summary>
        /// Intiates and creates a class based on a model of a bullet
        /// </summary>
        /// <param name="bullet">The model of the bullet holding the information</param>
        public void CreateClass(Bullet bullet)
        {
     
            base.CreateClass(bullet.Name, "PBullet", FrameWork.XNA); //Calling the masterclass method

            AddConstructor(bullet); //Adds the new Values to the constructor

            CookClass(); //Cooks the class and exports it to the destination specified
        }

        /// <summary>
        /// Adds the Constructor to the new Class to be generated
        /// </summary>
        private void AddConstructor(Bullet bullet)
        {
            CodeConstructor codeConstructor = new CodeConstructor();
            codeConstructor.Attributes = MemberAttributes.Public | MemberAttributes.Final;

            AddVariablesToConstructor(codeConstructor, bullet); //Adding The variables to the Constructor

            targetClass.Members.Add(codeConstructor);
        }

        /// <summary>
        /// Adds The Variables to the Constructor
        /// </summary>
        /// <param name="codeConstructor">The Constructor to be created</param>
        /// <param name="bullet">The bullet Model holding the Data</param>
        private void AddVariablesToConstructor(CodeConstructor codeConstructor, Bullet bullet)
        {
            #region Variables

            CodeFieldReferenceExpression Id =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "id");
            codeConstructor.Statements.Add(new CodeAssignStatement(Id,
                new CodeSnippetExpression(bullet.Id.ToString())));

            CodeFieldReferenceExpression Image =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_Image");
            codeConstructor.Statements.Add(new CodeAssignStatement(Image,
                new CodePrimitiveExpression(bullet.Image)));

            CodeFieldReferenceExpression Name =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_Name");
            codeConstructor.Statements.Add(new CodeAssignStatement(Name,
                new CodePrimitiveExpression(bullet.Name)));

            CodeFieldReferenceExpression Width =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_Width");
            codeConstructor.Statements.Add(new CodeAssignStatement(Width,
                new CodeSnippetExpression(bullet.Width.ToString())));

            CodeFieldReferenceExpression Height =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_Height");
            codeConstructor.Statements.Add(new CodeAssignStatement(Height,
                new CodeSnippetExpression(bullet.Height.ToString())));

            CodeFieldReferenceExpression HitPoints =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_HitPoints");
            codeConstructor.Statements.Add(new CodeAssignStatement(HitPoints,
                new CodeSnippetExpression(bullet.HitPoints.ToString())));

            CodeFieldReferenceExpression XVelocity =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_XVel");
            codeConstructor.Statements.Add(new CodeAssignStatement(XVelocity,
                new CodeSnippetExpression(bullet.XVelocity.ToString() + "f")));

            CodeFieldReferenceExpression YVelocity =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_YVel");
            codeConstructor.Statements.Add(new CodeAssignStatement(YVelocity,
                new CodeSnippetExpression(bullet.YVelocity.ToString() + "f")));

            CodeFieldReferenceExpression XAccelaration =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_YAcceleration");
            codeConstructor.Statements.Add(new CodeAssignStatement(XAccelaration,
                new CodeSnippetExpression(bullet.XAccelaration.ToString() + "f")));

            CodeFieldReferenceExpression YAccelaration =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_XAcceleration");
            codeConstructor.Statements.Add(new CodeAssignStatement(YAccelaration,
                new CodeSnippetExpression(bullet.YAccelaration.ToString() + "f")));

            CodeFieldReferenceExpression Damage =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_Damage");
            codeConstructor.Statements.Add(new CodeAssignStatement(Damage,
                new CodeSnippetExpression(bullet.Damage.ToString() + "f")));

            CodeFieldReferenceExpression BBoxXpercent =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bul_BBoxXpercent");
            codeConstructor.Statements.Add(new CodeAssignStatement(BBoxXpercent,
                new CodeSnippetExpression(bullet.BBoxXpercent.ToString() + "f")));

            CodeFieldReferenceExpression BBoxYpercent =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "bul_BBoxYpercent");
            codeConstructor.Statements.Add(new CodeAssignStatement(BBoxYpercent,
                new CodeSnippetExpression(bullet.BBoxYpercent.ToString() + "f")));

            CodeFieldReferenceExpression Radius =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "bul_Radius");
            codeConstructor.Statements.Add(new CodeAssignStatement(Radius,
                new CodeSnippetExpression(bullet.Radius.ToString() + "f")));

            CodeFieldReferenceExpression ExplodeAnimaton =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "bul_ExplodeAnimation");
            codeConstructor.Statements.Add(new CodeAssignStatement(ExplodeAnimaton,
                new CodeSnippetExpression(bullet.ExplodeAnimation.ToString())));

            CodeFieldReferenceExpression ExplodeAnimationSize =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "bul_ExplodeAnimationSize");
            codeConstructor.Statements.Add(new CodeAssignStatement(ExplodeAnimationSize,
                new CodeSnippetExpression(bullet.ExplodeAnimationSize.ToString())));

            #endregion
        }

    }
}
