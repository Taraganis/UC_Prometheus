﻿///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
///////////////////////////////////////////////////////
using UC_Prometheus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UC_Prometheus;

namespace UC_Prometheus.Code_Generation
{
    public static class Code_Factory
    {
        /// <summary>
        /// Generates a Screen Based on a Screen Model
        /// </summary>
        /// <param name="screen">The Screen Model to be parsed into a C# class</param>
        private static void GenerateScreen(Screen screen)
        {
            try
            {

                Screen_Generator sg = new Screen_Generator();
                sg.CreateClass(screen);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
        /// <summary>
        /// Getnerates a Player Based on a Player Model
        /// </summary>
        /// <param name="player">The Player model to be parsed to C# class</param>
        private static void GeneratePlayer(Player player)
        {
            try
            {
             //   Screen_Generator.Create_Screen(screen);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
        /// <summary>
        /// Generates a Bullet Based on a Bullet Model
        /// </summary>
        /// <param name="bullet">The Bullet model to be parsed to C# class</param>
        private static void GenerateBullet(Bullet bullet)
        {
            try
            {
              //  Screen_Generator.Create_Screen(screen);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
        /// <summary>
        /// Generates a Creep Based on a Creep Model
        /// </summary>
        /// <param name="creep">The Creep model to be parsed to C# class</param>
        private static void GenerateCreep(Creep creep)
        {
            try
            {
               // Screen_Generator.Create_Screen(screen);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
        /// <summary>
        /// Generates a Level Based on a Level Model
        /// </summary>
        /// <param name="level">The Level model to be parsed to C# class</param>
        private static void GenerateLevel(Level level)
        {
            try
            {
               // Screen_Generator.Create_Screen(level);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
        /// <summary>
        /// Generates a Parameters class Based on A Parameters Clas Model
        /// </summary>
        /// <param name="parameters">The Parameter class model to be parsed to C# class</param>
        private static void GenerateParameters(Parameters parameters)
        {
            try
            {
              //  Screen_Generator.Create_Screen(screen);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }

        /// <summary>
        /// Creates A game based on previously fed parameters
        /// </summary>
        public static void CookGame()
        {
            
           for (int i = 0; i < DataBank.fullGame.screens.Count(); i++)   //A loop that generates the screen classes
           {
                 GenerateScreen(DataBank.fullGame.screens[i]);
           }
            
           for(int i = 0;i<DataBank.fullGame.levels.Count();i++)     //A loop that generates the levels of the game
           {
                GenerateLevel(DataBank.fullGame.levels[i]);
           }


        }


    }
}
