﻿////////////////////////////////////////////////////////////
///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
//// //////////////////////////////////////////////////  
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.CodeDom.Compiler;
using System.IO;
using UC_Prometheus.Models;
using UC_Prometheus;


namespace UC_Prometheus.Code_Generation
{
    public class Creep_Generator : Generation_Base
    {

        /// Initializes the Instance in order to generator Code for a Creep
        /// </summary>
        /// <param name="name">The name of the class</param>
        public void CreateClass(Creep creepFactory)
        {
            base.CreateClass(creepFactory.Name, "PCreepFactory", FrameWork.XNA); //Initiate The Creation of the Class

            AddConstructor(creepFactory); //Adds the new Values to the constructor

            CookClass(); //Cooks the class and exports it to the destination specified

        }

        #region Private Methods

        /// <summary>
        /// Adds the constuctor to the class to be generated
        /// </summary>
        /// <param name="window"></param>
        private void AddConstructor(Creep creep)
        {
            CodeConstructor codeConstructor = new CodeConstructor(); //Initiating the Constructor Model
            codeConstructor.Attributes = MemberAttributes.Public | MemberAttributes.Final; //Adding the Member Attributes

            AddVariablesToConstructor(codeConstructor, creep);  //Adding the Variables to the Constructor

            targetClass.Members.Add(codeConstructor); //Adding the Constructor to the Target Class Model

        }

        /// <summary>
        /// Adding the variables to the constructor
        /// </summary>
        /// <param name="codeConstructor">The Codeconstructor model for the generation of the class</param>
        /// <param name="creep">The creep Model Holding the information</param>
        private void AddVariablesToConstructor(CodeConstructor codeConstructor, Creep creep)
        {
            #region Adding all things to the Constructor

            CodeFieldReferenceExpression id =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "id");
            codeConstructor.Statements.Add(new CodeAssignStatement(id,
                new CodeSnippetExpression(creep.Id.ToString())));

            CodeFieldReferenceExpression creep_name =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creep_name");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_name,
                new CodePrimitiveExpression(creep.Name)));

            CodeFieldReferenceExpression creep_Image =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creep_Image");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_Image,
                new CodePrimitiveExpression(@"Creeps\" + creep.Image)));

            CodeFieldReferenceExpression creep_healthBar =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "creep_healthBar");

            if (creep.HealthBar == true)
            {
                codeConstructor.Statements.Add(new CodeAssignStatement(creep_healthBar,
                new CodeSnippetExpression("true")));
            }
            else
            {
                codeConstructor.Statements.Add(new CodeAssignStatement(creep_healthBar,
                new CodeSnippetExpression("false")));
            }


            CodeFieldReferenceExpression creep_healthBar_offset =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "creep_healthBar_offset");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_healthBar_offset,
                new CodeSnippetExpression("new Vector2(" + creep.HealthBarOffset.x.ToString()
                + ", " + creep.HealthBarOffset.y.ToString() + ")")));

            CodeFieldReferenceExpression creep_Width =
              new CodeFieldReferenceExpression(
              new CodeThisReferenceExpression(), "creep_Width");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_Width,
                new CodeSnippetExpression(creep.Width.ToString())));

            CodeFieldReferenceExpression creep_BBoxXpercent =
              new CodeFieldReferenceExpression(
              new CodeThisReferenceExpression(), "creep_BBoxXpercent");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_BBoxXpercent,
                new CodeSnippetExpression(creep.BBoxXpercent.ToString())));

            CodeFieldReferenceExpression creep_BBoxYpercent =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_BBoxYpercent");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_BBoxYpercent,
                new CodeSnippetExpression(creep.BBoxYpercent.ToString())));

            CodeFieldReferenceExpression creep_Height =
              new CodeFieldReferenceExpression(
              new CodeThisReferenceExpression(), "creep_Height");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_Height,
                new CodeSnippetExpression(creep.Height.ToString())));

            CodeFieldReferenceExpression creep_pos =
              new CodeFieldReferenceExpression(
              new CodeThisReferenceExpression(), "creep_pos");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_pos,
                new CodeSnippetExpression("new Rectangle(" + creep.Position.x.ToString() + ","
                + creep.Position.y.ToString() + "," + creep.Position.width.ToString() + ","
                + creep.Position.height.ToString() + ")")));

            CodeFieldReferenceExpression creep_secondsTillActivate =
              new CodeFieldReferenceExpression(
              new CodeThisReferenceExpression(), "creep_secondsTillActivate");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_secondsTillActivate,
                new CodeSnippetExpression(creep.SecondTillActive.ToString())));

            CodeFieldReferenceExpression creep_secondsToBeActive =
              new CodeFieldReferenceExpression(
              new CodeThisReferenceExpression(), "creep_secondsToBeActive");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_secondsToBeActive,
                new CodeSnippetExpression(creep.SecondsToBeActive.ToString())));

            CodeFieldReferenceExpression creep_warningText =
              new CodeFieldReferenceExpression(
              new CodeThisReferenceExpression(), "creep_warningText");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_warningText,
                new CodePrimitiveExpression(creep.WarningMessage.ToString())));

            CodeFieldReferenceExpression creep_secondsBetweenMakeEventsMin =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_secondsBetweenMakeEventsMin");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_secondsBetweenMakeEventsMin,
                new CodeSnippetExpression(creep.SecondsBetweenEventsMin.ToString())));

            CodeFieldReferenceExpression creep_secondsBetweenMakeEventsMax =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_secondsBetweenMakeEventsMax");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_secondsBetweenMakeEventsMax,
                new CodeSnippetExpression(creep.SecondsBetweenEventsMax.ToString())));

            CodeFieldReferenceExpression creep_NumberToMakeMin =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_NumberToMakeMin");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_NumberToMakeMin,
                new CodeSnippetExpression(creep.NumberToMakeMin.ToString())));

            CodeFieldReferenceExpression creep_NumberToMakeMax =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_NumberToMakeMax");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_NumberToMakeMax,
                new CodeSnippetExpression(creep.NumberToMakeMax.ToString())));

            CodeFieldReferenceExpression creep_totalNumberToMake =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_totalNumberToMake");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_totalNumberToMake,
                new CodeSnippetExpression(creep.TotalNumberToMake.ToString())));

            CodeFieldReferenceExpression creep_HitPoints =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_HitPoints");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_HitPoints,
                new CodeSnippetExpression(creep.HitPoints.ToString())));

            CodeFieldReferenceExpression creep_Score =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_Score");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_Score,
                new CodeSnippetExpression(creep.Score.ToString())));

            CodeFieldReferenceExpression creep_ExplodeAnimation =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_ExplodeAnimation");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_ExplodeAnimation,
                new CodeSnippetExpression(creep.ExplodeAnimation.ToString())));

            CodeFieldReferenceExpression creep_ExplodeAnimationSize =
             new CodeFieldReferenceExpression(
             new CodeThisReferenceExpression(), "creep_ExplodeAnimationSize");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_ExplodeAnimationSize,
                new CodeSnippetExpression(creep.ExplodeAnimationSize.ToString())));

            CodeFieldReferenceExpression creep_bomb_id =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb_id");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb_id,
                new CodeSnippetExpression(creep.BombId.ToString())));

            CodeFieldReferenceExpression creep_bomb_secondsBetweenMakeEventsMin =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb_secondsBetweenMakeEventsMin");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb_secondsBetweenMakeEventsMin,
                new CodeSnippetExpression(creep.Bomb_SecondsBetweenMakeEventsMin.ToString())));

            CodeFieldReferenceExpression creep_bomb_secondsBetweenMakeEventsMax =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb_secondsBetweenMakeEventsMax");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb_secondsBetweenMakeEventsMax,
                new CodeSnippetExpression(creep.Bomb_SecondsBetweenMakeEventsMax.ToString())));

            CodeFieldReferenceExpression creep_bomb0_angle =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb0_angle");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb0_angle,
                new CodeSnippetExpression(creep.Bomb0_Angle.ToString() + "f")));

            CodeFieldReferenceExpression creep_bomb1_angle =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb1_angle");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb1_angle,
                new CodeSnippetExpression(creep.Bomb1_Angle.ToString() + "f")));

            CodeFieldReferenceExpression creep_bomb2_angle =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb2_angle");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb2_angle,
                new CodeSnippetExpression(creep.Bomb2_Angle.ToString() + "f")));

            CodeFieldReferenceExpression creep_bomb_speed0 =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb_speed0");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb_speed0,
                new CodeSnippetExpression(creep.Bomb_Speed0.ToString() + "f")));

            CodeFieldReferenceExpression creep_bomb_speed1 =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb_speed1");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb_speed1,
                new CodeSnippetExpression(creep.Bomb_Speed1.ToString() + "f")));

            CodeFieldReferenceExpression creep_bomb_speed2 =
            new CodeFieldReferenceExpression(
            new CodeThisReferenceExpression(), "creep_bomb_speed2");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_bomb_speed2,
                new CodeSnippetExpression(creep.Bomb_Speed2.ToString() + "f")));

            CodeFieldReferenceExpression creep_Damage =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creep_Damage");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_Damage,
                new CodeSnippetExpression(creep.Damage.ToString() + "f")));

            CodeFieldReferenceExpression creep_XVelMin =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creep_XVelMin");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_XVelMin,
                new CodeSnippetExpression(creep.XVelMin.ToString() + "f")));

            CodeFieldReferenceExpression creep_YVelMin =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creep_YVelMin");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_YVelMin,
                new CodeSnippetExpression(creep.YVelMax.ToString() + "f")));

            CodeFieldReferenceExpression creep_XVelMax =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creep_XVelMax");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_XVelMax,
                new CodeSnippetExpression(creep.YVelMax.ToString() + "f")));

            CodeFieldReferenceExpression creep_YVelMax =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creep_YVelMax");
            codeConstructor.Statements.Add(new CodeAssignStatement(creep_YVelMax,
                new CodeSnippetExpression(creep.YVelMax.ToString() + "f")));

            #endregion
        }

        #endregion

    }
}
