﻿////////////////////////////////////////////////////////////
///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
//// //////////////////////////////////////////////////  
using System;
using System.CodeDom;
using System.Windows;
using System.CodeDom.Compiler;
using System.IO;
using UC_Prometheus.Models;
using UC_Prometheus;

namespace UC_Prometheus.Code_Generation
{
    public class Screen_Generator : Generation_Base
    {

        /// <summary>
        /// Creating the instance of a class based on a Screen Model
        /// </summary>
        /// <param name="screen">The Screen Model to be parsed into a class</param>
        public void CreateClass(Screen screen)
        {

            base.CreateClass(screen.Name, "PScreenDisplay", FrameWork.XNA); //Starts the Initiation of the Class

          

            AddConstructor(screen); //Adds the Constructor to the class

            CookClass(); //Cooks the class

        }

        /// <summary>
        /// Adds the constructor for the screen based on a screen model
        /// </summary>
        /// <param name="screen"></param>
        private void AddConstructor(Screen screen)
        {
            
            CodeConstructor codeConstructor = new CodeConstructor(); //Initiating the Constructor
            codeConstructor.Attributes = MemberAttributes.Public | MemberAttributes.Final; //Assigning The Member Attributes

            //screenType = ScreenType.Menu;

            AddVariablesToConstructor(codeConstructor,screen);  //Adding the Variables to the Constructor

        }

        /// <summary>
        /// Assigns the Variables of Resolution to a Constructor
        /// </summary>
        /// <param name="codeConstructor"></param>
        private void AssignResolutionVariables(CodeConstructor codeConstructor)
        {
            if (DataBank.fullGame.ScreenWidth == 800)
            {
                CodeFieldReferenceExpression continueButtonPos =
                    new CodeFieldReferenceExpression(
                    new CodeThisReferenceExpression(), "continueButtonPos");
                codeConstructor.Statements.Add(new CodeAssignStatement(continueButtonPos,
                     new CodeSnippetExpression("new Vector2(700, 550)")));
            }
            else if (DataBank.fullGame.ScreenWidth == 640)
            {
                CodeFieldReferenceExpression continueButtonPos =
                    new CodeFieldReferenceExpression(
                    new CodeThisReferenceExpression(), "continueButtonPos");
                codeConstructor.Statements.Add(new CodeAssignStatement(continueButtonPos,
                     new CodeSnippetExpression("new Vector2(540, 430)")));
            }
            else
            {
                CodeFieldReferenceExpression continueButtonPos =
                    new CodeFieldReferenceExpression(
                    new CodeThisReferenceExpression(), "continueButtonPos");
                codeConstructor.Statements.Add(new CodeAssignStatement(continueButtonPos,
                     new CodeSnippetExpression("new Vector2(924, 698)")));
            }
        }

        /// <summary>
        /// Adding the Variables to the Constructor
        /// </summary>
        /// <param name="codeConstructor">The Constructor to Add The Varialbes</param>
        /// <param name="screen">The Model of the Screen Holding the information</param>
        private void AddVariablesToConstructor(CodeConstructor codeConstructor, Screen screen)
        {

            CodeFieldReferenceExpression idRef =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "id");
            codeConstructor.Statements.Add(new CodeAssignStatement(idRef,
                new CodeSnippetExpression("0")));

            CodeFieldReferenceExpression bigText =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bigText");
            codeConstructor.Statements.Add(new CodeAssignStatement(bigText,
                new CodePrimitiveExpression(screen.BigText)));

            CodeFieldReferenceExpression medText0 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "medText0");
            codeConstructor.Statements.Add(new CodeAssignStatement(medText0,
                new CodePrimitiveExpression(screen.MediumText)));

            CodeFieldReferenceExpression medText1 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "medText1");
            codeConstructor.Statements.Add(new CodeAssignStatement(medText1,
                new CodePrimitiveExpression(screen.MediumText2)));

            CodeFieldReferenceExpression medText2 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "medText2");
            codeConstructor.Statements.Add(new CodeAssignStatement(medText2,
                new CodePrimitiveExpression(screen.MediumText3)));

            CodeFieldReferenceExpression medText3 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "medText2");
            codeConstructor.Statements.Add(new CodeAssignStatement(medText2,
                new CodePrimitiveExpression("")));

            CodeFieldReferenceExpression BigTextPos =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "BigTextPos");
            codeConstructor.Statements.Add(new CodeAssignStatement(BigTextPos,
                new CodeSnippetExpression("new Vector2(100, 100)")));

            CodeFieldReferenceExpression medTextPos =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "medTextPos");
            codeConstructor.Statements.Add(new CodeAssignStatement(medTextPos,
                new CodeSnippetExpression("new Vector2(300, 50)")));

            CodeFieldReferenceExpression centerMedText =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "centerMedText");
            codeConstructor.Statements.Add(new CodeAssignStatement(centerMedText,
                new CodeSnippetExpression("true")));

            CodeFieldReferenceExpression continueText =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "continueText");
            codeConstructor.Statements.Add(new CodeAssignStatement(continueText,
                new CodePrimitiveExpression(screen.ContinueText)));

            CodeFieldReferenceExpression mouseImage =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "mouseImage");
            codeConstructor.Statements.Add(new CodeAssignStatement(mouseImage,
                new CodePrimitiveExpression("Mouse\\" + screen.MouseImage)));

            CodeFieldReferenceExpression buttonImage =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "buttonImage");
            codeConstructor.Statements.Add(new CodeAssignStatement(buttonImage,
                new CodePrimitiveExpression("GUI\\" + screen.ButtonImage)));

            CodeFieldReferenceExpression buttonSize =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "buttonSize");
            codeConstructor.Statements.Add(new CodeAssignStatement(buttonSize,
                 new CodeSnippetExpression("new Vector2(128,32)")));

            CodeFieldReferenceExpression splashImage =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "splashImage");
            codeConstructor.Statements.Add(new CodeAssignStatement(splashImage,
                 new CodePrimitiveExpression("Backgrounds\\" + screen.Image)));

            CodeFieldReferenceExpression splashColor =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "splashColor");
            codeConstructor.Statements.Add(new CodeAssignStatement(splashColor,
                 new CodeSnippetExpression("Color.White")));

            CodeFieldReferenceExpression splashBorder =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "splashBorder");
            codeConstructor.Statements.Add(new CodeAssignStatement(splashBorder,
                 new CodePrimitiveExpression("Intro\\" + screen.Border)));

            CodeFieldReferenceExpression splashBorderColor =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "splashBorderColor");
            codeConstructor.Statements.Add(new CodeAssignStatement(splashBorderColor,
                 new CodeSnippetExpression("Color.White")));

            CodeFieldReferenceExpression textColor =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "textColor");
            codeConstructor.Statements.Add(new CodeAssignStatement(textColor,
                 new CodeSnippetExpression("Color.White")));

            CodeFieldReferenceExpression key1ToContinue =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "key1ToContinue");
            codeConstructor.Statements.Add(new CodeAssignStatement(key1ToContinue,
                 new CodeSnippetExpression("Keys.Enter")));

            CodeFieldReferenceExpression key2ToContinue =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "key2ToContinue");
            codeConstructor.Statements.Add(new CodeAssignStatement(key2ToContinue,
                 new CodeSnippetExpression("Keys.Enter")));

            AssignResolutionVariables(codeConstructor);

            CodeFieldReferenceExpression nextLevelNum =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "nextLevelNum");
            codeConstructor.Statements.Add(new CodeAssignStatement(nextLevelNum,
                 new CodeSnippetExpression("2")));

            targetClass.Members.Add(codeConstructor);

        }
    }
}

