﻿////////////////////////////////////////////////////////////
///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
//// //////////////////////////////////////////////////  
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.CodeDom.Compiler;
using System.IO;
using UC_Prometheus.Models;
using UC_Prometheus;

/// <summary>
/// Creates a new class of PLevel type
/// </summary>
namespace UC_Prometheus.Code_Generation
{
    public class Level_Generator : Generation_Base
    {
    
        /// <summary>
        /// Adds the data to the model Class to be produced
        /// </summary>
        /// <param name="level">The level that needs to be saved</param>
        public void CreateClass(Level level)
        {

            base.CreateClass(level.Name, "PLevel",FrameWork.XNA); //Adding Values to the constructor

            AddConstructor(level); //Adds the new Values to the constructor

            CookClass(); //Cooks the class and exports it to the destination specified

        }

        /// <summary>
        /// Adds a Constructor based on the Values of A Level_Factory Form
        /// </summary>
        /// <param name="window"></param>
        private void AddConstructor(Level level)
        {

            CodeConstructor codeConstructor = new CodeConstructor(); //The code constructor to hold the data
            codeConstructor.Attributes = MemberAttributes.Public | MemberAttributes.Final; //Assigning the MemberAttributes

            AddVariablesToConstructor(codeConstructor,level); //Adding The Variables to the constructor

            targetClass.Members.Add(codeConstructor); //Addign the constructor to the targetClass

        }

        /// <summary>
        /// Adding the variables to the constructor
        /// </summary>
        /// <param name="codeConstructor">The constructor to Add the variables</param>
        /// <param name="level">The level model holding the attributes</param>
        private void AddVariablesToConstructor(CodeConstructor codeConstructor,Level level)
        {
            #region Variables

            CodeFieldReferenceExpression id =
              new CodeFieldReferenceExpression(
              new CodeThisReferenceExpression(), "id");
            codeConstructor.Statements.Add(new CodeAssignStatement(id,
                new CodeSnippetExpression(level.Id.ToString())));

            CodeFieldReferenceExpression background =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "background");
            codeConstructor.Statements.Add(new CodeAssignStatement(background,
                new CodePrimitiveExpression(level.Background)));

            CodeFieldReferenceExpression scroll_Speed =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "scrollSpeed");
            codeConstructor.Statements.Add(new CodeAssignStatement(scroll_Speed,
                new CodeSnippetExpression(level.ScrollSpeed.ToString() + "f")));

            CodeFieldReferenceExpression showExplodeRadius =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "showExplodeRadius");
            codeConstructor.Statements.Add(new CodeAssignStatement(showExplodeRadius,
                new CodeSnippetExpression("false")));

            CodeFieldReferenceExpression player_Image =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_Image");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_Image,
                new CodePrimitiveExpression(level.player.PlayerImage)));

            CodeFieldReferenceExpression player_hitPoints =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_hitPoints");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_hitPoints,
                new CodeSnippetExpression(level.player.HitPoints.ToString())));

            CodeFieldReferenceExpression player_leftVel =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_leftVel");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_leftVel,
                new CodeSnippetExpression(level.player.LeftVelocity.ToString())));

            CodeFieldReferenceExpression player_rightVel =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_rightVel");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_rightVel,
                new CodeSnippetExpression(level.player.RightVelocity.ToString())));


            CodeFieldReferenceExpression player_upVel =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_upVel");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_upVel,
                new CodeSnippetExpression(level.player.UpVelocity.ToString())));

            CodeFieldReferenceExpression player_downVel =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_downVel");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_downVel,
                new CodeSnippetExpression(level.player.DownVelocity.ToString())));

            CodeFieldReferenceExpression player_ammoMax =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_ammoMax");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_ammoMax,
                new CodeSnippetExpression(level.player.MaxAmmo.ToString())));

            CodeFieldReferenceExpression player_ammoTicks =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_ammoTicks");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_ammoTicks,
                new CodeSnippetExpression(level.player.Time.ToString())));


            CodeFieldReferenceExpression player_BBoxXpercent =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_BBoxXpercent");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_BBoxXpercent,
                new CodeSnippetExpression(level.player.BBoxXpercent.ToString())));

            CodeFieldReferenceExpression player_BBoxYpercent =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_BBoxYpercent");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_BBoxYpercent,
                new CodeSnippetExpression(level.player.BBoxYpercent.ToString())));

            CodeFieldReferenceExpression player_Height =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_Height");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_Height,
                new CodeSnippetExpression(level.player.Height.ToString())));

            CodeFieldReferenceExpression player_Width =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_Width");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_Width,
                new CodeSnippetExpression(level.player.Width.ToString())));

            CodeFieldReferenceExpression player_ExplodeAnimation =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_ExplodeAnimation");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_ExplodeAnimation,
                new CodeSnippetExpression(level.player.ExplodeAnimation.ToString())));


            CodeFieldReferenceExpression player_ExplodeAnimationSize =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_ExplodeAnimationSize");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_ExplodeAnimationSize,
                new CodeSnippetExpression(level.player.ExplodeAnimationSize.ToString())));

            CodeFieldReferenceExpression player_bullet =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "player_bullet");
            codeConstructor.Statements.Add(new CodeAssignStatement(player_bullet,
                new CodeSnippetExpression(level.player.BulletId.ToString())));

            CodeFieldReferenceExpression playerZone =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "playerZone");
            codeConstructor.Statements.Add(new CodeAssignStatement(playerZone,
                new CodeSnippetExpression("new Rectangle(" + level.player.PlayerZone.x.ToString()
                + "," + level.player.PlayerZone.y.ToString() + "," + level.player.PlayerZone.width.ToString()
                + "," + level.player.PlayerZone.height.ToString() + ")")));

            CodeFieldReferenceExpression screenArea =
               new CodeFieldReferenceExpression(
               new CodeThisReferenceExpression(), "screenArea");
            codeConstructor.Statements.Add(new CodeAssignStatement(screenArea,
                new CodeSnippetExpression("new Rectangle(0, 0, G.screenWidth, G.ScreenHeight)")));

            #endregion
        }

    }

 }


