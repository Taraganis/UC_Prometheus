﻿///Basic Code Generating Class For the UC Game Generator //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
///////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.AccessControl;
using UC_Prometheus.Models;

namespace UC_Prometheus.Code_Generation
{
    /// <summary>
    /// An Object Class for reading and modifying large documents
    /// </summary>
    public class DirectoryUtilities
    {

        /// <summary>
        /// Creates the Directory for the Classes
        /// </summary>
        /// <param name="folderName">The Name of the game that will be the name of the folder as well</param>
        /// <returns></returns>
        public static bool CreateDirectory(string folderName)
        {

            bool isSuccesful = false;

            DataBank.GameName = folderName;

            string directory = Directory.GetCurrentDirectory() +
                "\\Output\\" + folderName;

            if(Directory.Exists(directory))
            {
                directory = directory + DateTime.Now.Second.ToString();
            }

            #region Creating a Directory for the game and copying stuff

            try
            {
                Directory.CreateDirectory(directory);
                DataBank.GameDirectory = directory;

                //DirectoryInfo dir = new DirectoryInfo(@"Resources\Classes\");
                //CopyFiles(directory, @"Resources\Classes\");

                CopyClasses(directory);

                CopyResources(directory);

                Directory.CreateDirectory(directory + "\\Content\\");
                
                CopyFiles(directory + "\\Content\\", @"Resources\Content\");

                isSuccesful = true;
            }
            catch(Exception e)
            {
                isSuccesful = false;
            }
            #endregion

            return isSuccesful;
        }
        
        /// <summary>
        /// Creates the game Based on the model in the Prometheus Databank
        /// </summary>
        /// <returns></returns>
        public static bool CreateGame()
        {

            bool isComplete = false;    //Boolean for checking if the process was done

            try
            {
                foreach (Screen screen in DataBank.fullGame.screens)
                {
                    Screen_Generator sg = new Screen_Generator();
                    sg.CreateClass(screen);
                }   
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            try
            {
                foreach (Level level in DataBank.fullGame.levels)
                {
                    Level_Generator lg = new Level_Generator();
                    lg.CreateClass(level);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
                
            }
            try
            {
                foreach (Creep creep in DataBank.fullGame.creeps)
                {
                    Creep_Generator cg = new Creep_Generator();
                    cg.CreateClass(creep);
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            try
            {
                foreach (Bullet bullet in DataBank.fullGame.bullets)
                {
                    Bullet_Generator bg = new Bullet_Generator();
                    bg.CreateClass(bullet);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            try
            {
                ParameterClass_Gereration pg = new ParameterClass_Gereration();
                pg.CreateClass(DataBank.fullGame);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);

            }
            isComplete = true;

            return isComplete;
        }
        /// <summary>
        /// Coppies the resources to the output folder
        /// </summary>
        /// <returns></returns>
        public static bool CopyResources(string directory)
        {
            bool isSuccesful = false;

            try
            {
                Directory.CreateDirectory(directory + "\\Resources\\");
                DirectoryInfo dir = new DirectoryInfo(@"Resources");
                DirectoryInfo[] dirs = dir.GetDirectories();
                foreach(DirectoryInfo subDir in dirs)
                {
                    if(subDir.Name != "XML" && subDir.Name != "Classes")
                    {
                        Directory.CreateDirectory(directory + "\\Resources\\" + subDir.Name);
                        CopyFiles(directory + "\\Resources\\" + subDir.Name, subDir.FullName);
                    }
                   
                }
                
            }
            catch(Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return isSuccesful;
        }
        /// <summary>
        /// Returns the file names of the classes
        /// </summary>
        /// <returns></returns>
        public static List<string> ReturnClassNames()
        {
            #region Methods

            List<string> toReturn = new List<string>();

            try
            {

                DirectoryInfo dir = new DirectoryInfo(DataBank.GameDirectory);
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string[] splitter = file.Name.Split('.');
                    toReturn.Add(splitter[0]);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
            return toReturn;
#endregion
        }

        public static bool CopyClasses(string directoryTo)
        {
            bool isSucessful = false;

            string directoryFrom = @"Resources\Classes\";

            #region Creating a Directory for the game and copying stuff

            try
            {

                DirectoryInfo dir = new DirectoryInfo(directoryFrom);
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string tempName = Path.Combine(directoryTo, file.Name);
                    try
                    {
                        GameStream gameStreamer = new GameStream(DataBank.ProjectName, file.FullName, file.Name);
                        gameStreamer.GenerateClass();
                    }
                    catch (Exception ex) //In case of failure the model is reverted
                    {
                        Console.WriteLine("Unable to copy files");
                        Console.WriteLine(ex.StackTrace);
                        DataBank.fullGame = null;
                    }
                }
                isSucessful = true;
            }
            catch (Exception e)
            {
                isSucessful = false;
            }
            #endregion

            return isSucessful;
        }

        private static bool CopyContent(string directory)
        {
            bool isSuccesful = false;

            try
            {
                Directory.CreateDirectory(directory + "\\Content\\");
                DirectoryInfo dir = new DirectoryInfo(@"Resources\Content");
                FileInfo[] dirs = dir.GetFiles();
                foreach (FileInfo subDir in dirs)
                {
 
                    CopyFiles(directory + "\\Content\\", subDir.FullName);

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }

            return isSuccesful;
        }

        /// <summary>
        /// Copies the files from a directory to another directory
        /// </summary>
        /// <param name="directoryTo">The directory with the files</param>
        /// <param name="directoryFrom">The destination directory</param>
        /// <returns></returns>
        private static bool CopyFiles(string directoryTo,string directoryFrom)
        {
            bool isSuccesful = false;

            #region Creating a Directory for the game and copying stuff

            try
            {

                DirectoryInfo dir = new DirectoryInfo(directoryFrom);
                FileInfo[] files = dir.GetFiles();
                foreach (FileInfo file in files)
                {
                    string tempName = Path.Combine(directoryTo, file.Name);
                    try
                    {
                        file.CopyTo(tempName, true);
                    }
                    catch (Exception ex) //In case of failure the model is reverted
                    {
                        Console.WriteLine("Unable to copy files");
                        Console.WriteLine(ex.StackTrace);
                        DataBank.fullGame = null;
                    }
                }
                isSuccesful = true;
            }
            catch (Exception e)
            {
                isSuccesful = false;
            }
            #endregion
            return isSuccesful;

        }

        private static void TestModel()
        {
            
            



        }
    }
}
