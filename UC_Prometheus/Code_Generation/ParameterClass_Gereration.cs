﻿///////////////////////////////////////////////////////////
//Basic Code Generating Class For the UC Game Generator //
// Written by Michail Taraganiss                        //
// Suppervised by Robert Cox                          //
// Copyright 2015                                    //
/// //////////////////////////////////////////////////  
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.CodeDom.Compiler;
using System.IO;
using UC_Prometheus.Models;
using UC_Prometheus;

namespace UC_Prometheus.Code_Generation
{
    public class ParameterClass_Gereration : Generation_Base
    {
       

        /// <summary>
        /// Creates the instance of a Class based on a game model
        /// </summary>
        /// <param name="game">The game model for the creation of the game</param>
        public void CreateClass(FullGame game)
        {

            base.CreateClass("GameParamsClass", null, FrameWork.XNA);    //Initiates The Creation of the Class

            AddFields(game); //Adds the appropiate fields to the to be generated Class
            AddMethods(game); //Adds the appropriate methods to the to be generated Class

            CookClass(); //Cooks the class based on the model provided
        }


        #region Private Methods

        /// <summary>
        /// Adds Fields to the output Class (Really Long Method. I warn you xD)
        /// </summary>
        private void AddFields(FullGame game)
        {
            #region tempResourceDir

            CodeMemberField resources = new CodeMemberField();
            resources.Attributes = MemberAttributes.Public;
            resources.Name = "resources";
            resources.Type = new CodeTypeReference(typeof(string));
            resources.Comments.Add(new CodeCommentStatement("The Temporary Dir of resources"));
            resources.InitExpression = new CodePrimitiveExpression("\\Resources\\");
            targetClass.Members.Add(resources);

            CodeMemberField tempResourceDir = new CodeMemberField();
            tempResourceDir.Attributes = MemberAttributes.Public;
            tempResourceDir.Name = "tempResourceDir";
            tempResourceDir.Type = new CodeTypeReference(typeof(string));
            tempResourceDir.Comments.Add(new CodeCommentStatement("The Temporary Dir of resources"));
            tempResourceDir.InitExpression = new CodeSnippetExpression("Directory.GetCurrentDirectory()");
            targetClass.Members.Add(tempResourceDir);

            CodeMemberField resourceDir = new CodeMemberField();
            resourceDir.Attributes = MemberAttributes.Public;
            resourceDir.Name = "resourceDir";
            resourceDir.Type = new CodeTypeReference(typeof(string));
            resourceDir.Comments.Add(new CodeCommentStatement("Dir of resources"));
            resourceDir.InitExpression = new CodeSnippetExpression("Directory.GetCurrentDirectory()");
            targetClass.Members.Add(resourceDir);

            #endregion

            #region Fonts

            #region game_SmallFont 

            CodeMemberField game_SmallFont = new CodeMemberField();
            game_SmallFont.Attributes = MemberAttributes.Public;
            game_SmallFont.Name = "game_SmallFont";
            game_SmallFont.Type = new CodeTypeReference("SpriteFont", CodeTypeReferenceOptions.GenericTypeParameter);
            game_SmallFont.Comments.Add(new CodeCommentStatement("The Small Font"));
            game_SmallFont.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(game_SmallFont);

            #endregion

            #region game_MediumFont  

            CodeMemberField game_MediumFont = new CodeMemberField();
            game_MediumFont.Attributes = MemberAttributes.Public;
            game_MediumFont.Name = "game_MediumFont";
            game_MediumFont.Type = new CodeTypeReference("SpriteFont", CodeTypeReferenceOptions.GenericTypeParameter);
            game_MediumFont.Comments.Add(new CodeCommentStatement("The Medium Font"));
            game_MediumFont.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(game_MediumFont);

            #endregion

            #region game_BigFont  

            CodeMemberField game_BigFont = new CodeMemberField();
            game_BigFont.Attributes = MemberAttributes.Public;
            game_BigFont.Name = "game_BigFont";
            game_BigFont.Type = new CodeTypeReference("SpriteFont", CodeTypeReferenceOptions.GenericTypeParameter);
            game_BigFont.Comments.Add(new CodeCommentStatement("The Big Font"));
            game_BigFont.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(game_BigFont);

            #endregion

            #endregion

            #region Border

            CodeMemberField border = new CodeMemberField();
            border.Attributes = MemberAttributes.Public;
            border.Name = "border";
            border.Type = new CodeTypeReference(typeof(int));
            border.Comments.Add(new CodeCommentStatement("The Small Font"));
            targetClass.Members.Add(border);

            #endregion

            #region Screens

            #region Intro Screen

            CodeMemberField intro = new CodeMemberField();
            intro.Attributes = MemberAttributes.Public;
            intro.Name = "intro";
            intro.Type = new CodeTypeReference("PScreenDisplay", CodeTypeReferenceOptions.GenericTypeParameter);
            intro.Comments.Add(new CodeCommentStatement("level 0"));
            intro.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(intro);


            #endregion

            #region Win Screen

            CodeMemberField win = new CodeMemberField();
            win.Attributes = MemberAttributes.Public;
            win.Name = "win";
            win.Type = new CodeTypeReference("PScreenDisplay", CodeTypeReferenceOptions.GenericTypeParameter);
            win.Comments.Add(new CodeCommentStatement("level 4"));
            win.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(win);

            #endregion

            #region Fail Screen

            CodeMemberField fail = new CodeMemberField();
            fail.Attributes = MemberAttributes.Public;
            fail.Name = "fail";
            fail.Type = new CodeTypeReference("PScreenDisplay", CodeTypeReferenceOptions.GenericTypeParameter);
            fail.Comments.Add(new CodeCommentStatement("level 3"));
            fail.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(fail);


            #endregion

            #region Menu Screen

            CodeMemberField menu = new CodeMemberField();
            menu.Attributes = MemberAttributes.Public;
            menu.Name = "menu";
            menu.Type = new CodeTypeReference("PScreenDisplay", CodeTypeReferenceOptions.GenericTypeParameter);
            menu.Comments.Add(new CodeCommentStatement("level 1"));
            menu.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(menu);


            #endregion

            #region About Screen

            CodeMemberField about = new CodeMemberField();
            about.Attributes = MemberAttributes.Public;
            about.Name = "about";
            about.Type = new CodeTypeReference("PScreenDisplay", CodeTypeReferenceOptions.GenericTypeParameter);
            about.Comments.Add(new CodeCommentStatement("level 5"));
            about.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(about);


            #endregion

            #region Help1 Screen

            CodeMemberField help1 = new CodeMemberField();
            help1.Attributes = MemberAttributes.Public;
            help1.Name = "help1";
            help1.Type = new CodeTypeReference("PScreenDisplay", CodeTypeReferenceOptions.GenericTypeParameter);
            help1.Comments.Add(new CodeCommentStatement("level 6"));
            help1.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(help1);


            #endregion

            #region Help2 Screen

            CodeMemberField help2 = new CodeMemberField();
            help2.Attributes = MemberAttributes.Public;
            help2.Name = "help2";
            help2.Type = new CodeTypeReference("PScreenDisplay", CodeTypeReferenceOptions.GenericTypeParameter);
            help2.Comments.Add(new CodeCommentStatement("level 7"));
            help2.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(help2);


            #endregion

            #region Help3 Screen

            CodeMemberField help3 = new CodeMemberField();
            help3.Attributes = MemberAttributes.Public;
            help3.Name = "help3";
            help3.Type = new CodeTypeReference("PScreenDisplay", CodeTypeReferenceOptions.GenericTypeParameter);
            help3.Comments.Add(new CodeCommentStatement("level 8"));
            help3.InitExpression = new CodeSnippetExpression("null");
            targetClass.Members.Add(help3);


            #endregion

            #endregion

            #region Cheat Code

            CodeMemberField cheatCode = new CodeMemberField();
            cheatCode.Attributes = MemberAttributes.Public;
            cheatCode.Name = "cheatCode";
            cheatCode.Type = new CodeTypeReference(typeof(string));
            cheatCode.Comments.Add(new CodeCommentStatement("The Cheat Code Assigned"));
            cheatCode.InitExpression = new CodePrimitiveExpression("CHEAT");
            targetClass.Members.Add(cheatCode);

            #endregion

            #region First Level

            CodeMemberField firstPLevel = new CodeMemberField();
            firstPLevel.Attributes = MemberAttributes.Public;
            firstPLevel.Name = "firstPLevel";
            firstPLevel.Type = new CodeTypeReference(typeof(int));
            firstPLevel.InitExpression = new CodeSnippetExpression("0");
            firstPLevel.Comments.Add(new CodeCommentStatement("The Small Font"));
            targetClass.Members.Add(firstPLevel);

            #endregion

            #region Arrays

            #region LevelArray

            CodeMemberField lev = new CodeMemberField();
            lev.Attributes = MemberAttributes.Public;
            lev.Name = "lev";
            lev.Type = new CodeTypeReference("PLevel[]");
            lev.InitExpression = new CodeSnippetExpression("null");
            lev.Comments.Add(new CodeCommentStatement("The Level Array"));
            targetClass.Members.Add(lev);

            #endregion

            #region Creep Array

            CodeMemberField creeps  = new CodeMemberField();
            creeps.Attributes = MemberAttributes.Public;
            creeps.Name = "creeps";
            creeps.Type = new CodeTypeReference("PCreepFactory[]");
            creeps.InitExpression = new CodeSnippetExpression("null");
            creeps.Comments.Add(new CodeCommentStatement("The Creep Array"));
            targetClass.Members.Add(creeps);

            #endregion

            #region Bullet Array

            CodeMemberField bullets = new CodeMemberField();
            bullets.Attributes = MemberAttributes.Public;
            bullets.Name = "bullets";
            bullets.Type = new CodeTypeReference("PBullet[]");
            bullets.InitExpression = new CodeSnippetExpression("null");
            bullets.Comments.Add(new CodeCommentStatement("The Bullet Array"));
            targetClass.Members.Add(bullets);

            #endregion

            #endregion

            #region Constants

            #region MaxNumOfPLevels

            CodeMemberField maxNumOfPLevels  = new CodeMemberField();
            maxNumOfPLevels .Attributes = MemberAttributes.Public | MemberAttributes.Const;
            maxNumOfPLevels .Name = "maxNumOfPLevels ";
            maxNumOfPLevels .Type = new CodeTypeReference(typeof(int));
            maxNumOfPLevels .InitExpression = new CodeSnippetExpression(game.levels.Count().ToString());
            maxNumOfPLevels .Comments.Add(new CodeCommentStatement("The Max Number of Levels"));
            targetClass.Members.Add(maxNumOfPLevels );

            #endregion

            #region MaxNumOfPLevels

            CodeMemberField maxNumOfBullets= new CodeMemberField();
            maxNumOfBullets.Attributes = MemberAttributes.Public | MemberAttributes.Const;
            maxNumOfBullets.Name = "maxNumOfBullets";
            maxNumOfBullets.Type = new CodeTypeReference(typeof(int));
            maxNumOfBullets.InitExpression = new CodeSnippetExpression(game.bullets.Count().ToString());
            maxNumOfBullets.Comments.Add(new CodeCommentStatement("The Max Number of Bullets"));
            targetClass.Members.Add(maxNumOfBullets);

            #endregion

            #region MaxNumOfPLevels

            CodeMemberField maxNumOfCreeps  = new CodeMemberField();
            maxNumOfCreeps.Attributes = MemberAttributes.Public | MemberAttributes.Const;
            maxNumOfCreeps.Name = "maxNumOfCreeps";
            maxNumOfCreeps.Type = new CodeTypeReference(typeof(int));
            if(game.creeps.Count()>1)
                maxNumOfCreeps.InitExpression = new CodeSnippetExpression(game.creeps.Count().ToString());
            else
                maxNumOfCreeps.InitExpression = new CodeSnippetExpression(game.creeps.Count().ToString());
            maxNumOfCreeps.Comments.Add(new CodeCommentStatement("The Max Number of Creeps"));
            targetClass.Members.Add(maxNumOfCreeps);

            #endregion

            #endregion
        }

        /// <summary>
        /// Adds the constructor to the approprite class
        /// </summary>
        private void AddConstructor()
        {
            //This is intentionly empty. No Constructor is required for the game
            //But it was added for possible future deployment
        }

       
        private void AddMethods(FullGame game)
        {
            AddInitMethod(game);
            AddLoadLevels(game);
            AddLoadCreeps(game);
            AddLoadBullets(game);
        }

        #region Class Internal Methods

        /// <summary>
        /// Adds the Init Method to the class
        /// </summary>
        private void AddInitMethod(FullGame game)
        {
            // Declaring a ToString method
            CodeMemberMethod init = new CodeMemberMethod();
            init.Attributes =
                MemberAttributes.Public;
            init.Name = "init";
            init.ReturnType =
                new CodeTypeReference();

            CodeFieldReferenceExpression border =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "border");
            init.Statements.Add(new CodeAssignStatement(border,
                 new CodeSnippetExpression("40")));

            CodeFieldReferenceExpression tempResourceDir =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "tempResourceDir");
            init.Statements.Add(new CodeAssignStatement(tempResourceDir,
                 new CodeSnippetExpression("Directory.GetCurrentDirectory() + resources")));

            CodeFieldReferenceExpression resourceDir =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "resourceDir");
            init.Statements.Add(new CodeAssignStatement(resourceDir,
                 new CodeSnippetExpression("Directory.GetCurrentDirectory() + resources")));



            //CodeFieldReferenceExpression resourceDir =
            //    new CodeFieldReferenceExpression(
            //    new CodeThisReferenceExpression(), "border");
            //init.Statements.Add(new CodeAssignStatement(border,
            //     new CodeSnippetExpression("Directory.GetCurrentDirectory() + "\\Resources\\"")));

            #region Fonts

            CodeFieldReferenceExpression game_SmallFont =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "game_SmallFont");
            init.Statements.Add(new CodeAssignStatement(game_SmallFont,
                 new CodeSnippetExpression("G.fnt_courier12")));

            CodeFieldReferenceExpression game_MediumFont =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "game_MediumFont");
            init.Statements.Add(new CodeAssignStatement(game_MediumFont,
                 new CodeSnippetExpression("G.fnt_courier14")));

            CodeFieldReferenceExpression game_BigFont =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "game_BigFont");
            init.Statements.Add(new CodeAssignStatement(game_BigFont,
                 new CodeSnippetExpression("G.fnt_courier26")));

            #endregion

            #region Array Initiation

            CodeFieldReferenceExpression lev =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "lev");
            init.Statements.Add(new CodeAssignStatement(lev,
                 new CodeSnippetExpression("new PLevel[maxNumOfPLevels]")));

            CodeFieldReferenceExpression creeps =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creeps");
            init.Statements.Add(new CodeAssignStatement(creeps,
                 new CodeSnippetExpression("new PCreepFactory[maxNumOfCreeps]")));

            CodeFieldReferenceExpression bullets =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bullets");
            init.Statements.Add(new CodeAssignStatement(bullets,
                 new CodeSnippetExpression("new PBullet[maxNumOfBullets]")));

            #endregion

            #region Screen Initiation

            CodeFieldReferenceExpression intro =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "intro ");
            init.Statements.Add(new CodeAssignStatement(intro,
                 new CodeSnippetExpression("new Intro()")));

            CodeFieldReferenceExpression win =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "win");
            init.Statements.Add(new CodeAssignStatement(win,
                 new CodeSnippetExpression("new Win()")));

            CodeFieldReferenceExpression fail =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "fail");
            init.Statements.Add(new CodeAssignStatement(fail,
                 new CodeSnippetExpression("new Fail()")));

            CodeFieldReferenceExpression menu =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "menu");
            init.Statements.Add(new CodeAssignStatement(menu,
                 new CodeSnippetExpression("new Menu()")));

            CodeFieldReferenceExpression about =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "about");
            init.Statements.Add(new CodeAssignStatement(about,
                 new CodeSnippetExpression("new Menu()")));

            CodeFieldReferenceExpression help1 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "help1");
            init.Statements.Add(new CodeAssignStatement(help1,
                 new CodeSnippetExpression("new Menu()")));

            CodeFieldReferenceExpression help2 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "help2");
            init.Statements.Add(new CodeAssignStatement(help2,
                 new CodeSnippetExpression("new Menu()")));

            CodeFieldReferenceExpression help3 =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "help3");
            init.Statements.Add(new CodeAssignStatement(help3,
                 new CodeSnippetExpression("new Menu()")));


            #endregion

            #region Calling Loaders

            CodeSnippetExpression LoadBullets =
                new CodeSnippetExpression("LoadBullets()");
            init.Statements.Add(LoadBullets);

            CodeSnippetExpression LoadCreeps =
                new CodeSnippetExpression("LoadCreeps()");
            init.Statements.Add(LoadCreeps);

            CodeSnippetExpression LoadLevels =
                new CodeSnippetExpression("LoadLevels()");
            init.Statements.Add(LoadLevels);

            #endregion

           

            targetClass.Members.Add(init);

        }

        /// <summary>
        /// Adds the Load Level Method to the Class
        /// </summary>
        private void AddLoadLevels(FullGame game)
        {
            CodeMemberMethod loadLevels = new CodeMemberMethod();
            loadLevels.Attributes =
                MemberAttributes.Private;
            loadLevels.Name = "LoadLevels";
            loadLevels.ReturnType =
                new CodeTypeReference();

            for(int i = 0; i< game.levels.Count();i++)
            {
                    CodeFieldReferenceExpression level_Initiation =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "lev["+i.ToString()+"]");
                    loadLevels.Statements.Add(new CodeAssignStatement(level_Initiation,
                         new CodeSnippetExpression("new "+ game.levels[i].Name+"()")));
            }
            

            #endregion

            targetClass.Members.Add(loadLevels);
        }

        /// <summary>
        /// Adds the Load Creeps method to the class
        /// </summary>
        private void AddLoadCreeps(FullGame game)
        {
            CodeMemberMethod loadCreeps = new CodeMemberMethod();
            loadCreeps.Attributes =
                MemberAttributes.Private;
            loadCreeps.Name = "LoadCreeps";
            loadCreeps.ReturnType =
                new CodeTypeReference();

            #region Creeps

            for (int i = 0; i < game.creeps.Count(); i++)
            {
                
                    CodeFieldReferenceExpression creep_Initiation =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "creeps[" + i.ToString() + "]");
                    loadCreeps.Statements.Add(new CodeAssignStatement(creep_Initiation,
                         new CodeSnippetExpression("new " + game.creeps[i].Name + "()")));
            }


            #endregion

            

            targetClass.Members.Add(loadCreeps);
        }

        /// <summary>
        /// Adds the load bullets method to the class
        /// </summary>
        private void AddLoadBullets(FullGame game)
        {
            CodeMemberMethod loadBullets = new CodeMemberMethod();
            loadBullets.Attributes =
                MemberAttributes.Private;
            loadBullets.Name = "LoadBullets";
            loadBullets.ReturnType =
                new CodeTypeReference();

            for (int i = 0; i < game.bullets.Count; i++)
            {
                    CodeFieldReferenceExpression bullet_Initiation =
                new CodeFieldReferenceExpression(
                new CodeThisReferenceExpression(), "bullets[" + i.ToString() + "]");
                    loadBullets.Statements.Add(new CodeAssignStatement(bullet_Initiation,
                         new CodeSnippetExpression("new " + game.bullets[i].Name + "()")));
            }

            targetClass.Members.Add(loadBullets);
        }

        #endregion
    }
}
