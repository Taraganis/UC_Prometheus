﻿///Basic Code Generating Class For the UC Game Generator       //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using UC_Prometheus.Models;
using UC_Prometheus.Code_Generation;

namespace UC_Prometheus
{
    /// <summary>
    /// And enum representing the screen Type
    /// </summary>
    public enum ScreenType
    {
        Win, Fail, Menu, Intro
    };
    
    public class DataBank
    {

        #region Strings for Addresses And NameSpaces

        public static string ProjectName = "UCDemoByHand"; // The project name of the game
        public static string GameName = "Testing"; //The name of the game to be created
        public static string GameDirectory = "";

        #endregion



        public static FullGame fullGame;


    }
}
