﻿////////////////////////////////////////////////////////////
///Basic Model Class For the UC Game Generator           //
/// Written by Michail Taraganis                        //
/// Suppervised by Robert Cox                          //
/// Copyright 2015                                    //
//// //////////////////////////////////////////////////  
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UC_Prometheus.Models
{
    public class Player
    {
        #region Player Properties

        /// <summary>
        /// Gets of Sets The PlayerImage
        /// </summary>
        public string PlayerImage { get; set; }
        /// <summary>
        /// Gets of Sets The HitPoints
        /// </summary>
        public float HitPoints { get; set; }
        /// <summary>
        /// Gets or Sets The left Velocity
        /// </summary>
        public float LeftVelocity { get; set; }
        /// <summary>
        /// Gets or Sets The Right Velocity
        /// </summary>
        public float RightVelocity { get; set; }
        /// <summary>
        /// Gets or Sets the Up Velocity
        /// </summary>
        public float UpVelocity { get; set; }
        /// <summary>
        /// Gets or Sets The Down Velocity
        /// </summary>
        public float DownVelocity { get; set; }
        /// <summary>
        /// Gets or Sets The Max Ammount of Ammo on Screen
        /// </summary>
        public int MaxAmmo { get; set; }
        /// <summary>
        /// Gets or Sets The ticks between two shots
        /// </summary>
        public int Time { get; set; }
        /// <summary>
        /// The Bullets of the Player
        /// </summary>
        public Bullet Bullet { get; set; }

        public RectangleXNA PlayerZone { get; set; }

        public float BBoxXpercent { get; set; }

        public float BBoxYpercent { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        public int ExplodeAnimation { get; set; }

        public int ExplodeAnimationSize { get; set; }

        public int BulletId { get; set; }

        #endregion
    }
}
