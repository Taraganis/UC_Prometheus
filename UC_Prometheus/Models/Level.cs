﻿///Basic Code Generating Class Model For the UC Game Generator //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UC_Prometheus.Models
{
    public class Level
    {
        /// <summary>
        /// The Name of The level
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The Id of the Level
        /// </summary>
        public int Id{get;set;}
        /// <summary>
        /// Gets or Sets The Screen Area of the Level
        /// </summary>
        public RectangleXNA ScreenArea { get; set; }
        /// <summary>
        /// Gets or Sets the PlayerZone of the Level
        /// </summary>
        public string PlayerZone { get; set; }
        /// <summary>
        /// Gets or Sets the Background Image of the Level
        /// </summary>
        public string Background { get; set; }
        /// <summary>
        /// Gets or Sets the ScrollSpeed of the Level
        /// </summary>
        public float ScrollSpeed { get; set; }
        /// <summary>
        /// Gets or Sets the Boolean for the Explosion
        /// </summary>
        public bool showExplosion { get; set; }
        /// <summary>
        /// Gets or Sets the Player for the Level
        /// </summary>
        public Player player { get; set; }
    }
}
