﻿///Basic Code Generating Class Model For the UC Game Generator //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UC_Prometheus.Models
{
    public class Screen
    {
        #region Properties

        /// <summary>
        /// Gets or Sets the Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or Sets the Image
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// Gets or Sets the Big Text
        /// </summary>
        public string BigText { get; set; }
        /// <summary>
        /// Gets of Sets the Medium Text
        /// </summary>
        public string MediumText { get; set; }
        /// <summary>
        /// Gets or Sets the Medium Text2
        /// </summary>
        public string MediumText2 { get; set; }
        /// <summary>
        /// Gets or Sets the Medium Text 3
        /// </summary>
        public string MediumText3 { get; set; }
        /// <summary>
        /// Gets or Sets the Continue Text
        /// </summary>
        public string ContinueText { get; set; }
        /// <summary>
        /// Gets or Sets teh Mouse Image
        /// </summary>
        public string MouseImage { get; set; }
        /// <summary>
        /// Gets or Sets the Button Image
        /// </summary>
        public string ButtonImage { get; set; }
        /// <summary>
        /// Gets or Sets the Border Image
        /// </summary>
        public string Border { get; set; }

        #endregion
    }
}
