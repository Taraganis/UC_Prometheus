﻿///Basic Code Generating Class Model For the UC Game Generator //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UC_Prometheus.Models
{
    /// <summary>
    /// A Struct Based on the XNA Rectangle
    /// </summary>
    public struct RectangleXNA
    {
        public int y;
        public int width;
        public int x;
        public int height;
        
    }
    /// <summary>
    /// A Struct Based on The Vector2D of the XNA Framework
    /// </summary>
    public struct Vextor2XNA
    {
        public float x;
        public float y;
    }
    /// <summary>
    /// The Creep Model Class
    /// </summary>
    public class Creep
    {

        #region Properties

        /// <summary>
        /// Gets or Sets the ID of the Creep
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Gets or Sets the Name of the Creep
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or Sets the Image of the Creep
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// Gets or Sets the HealthBar Visibility 
        /// </summary>
        public bool HealthBar { get; set; }
        /// <summary>
        /// Gets or Sets the Width
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// Gets or Sets the Height
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// Gets or Sets the Ticks to be Active
        /// </summary>
        public int SecondTillActive { get; set; }
        /// <summary>
        /// Gets or Sets the Seconds that the Creep Will be Active
        /// </summary>
        public int SecondsToBeActive { get; set; }
        /// <summary>
        /// Gets or Sets the Warning Message
        /// </summary>
        public string WarningMessage { get; set; }
        /// <summary>
        /// Gets or Sets the Damage Delt by the Creep
        /// </summary>
        public float Damage { get; set; }
        /// <summary>
        /// Gets or Sets the Hit Points
        /// </summary>
        public int HitPoints { get; set; }
        /// <summary>
        /// Gets or Sets the Score achieved when the creep is killed
        /// </summary>
        public int Score { get; set; }
        /// <summary>
        /// Gets or Sets the Minimum X Velocity
        /// </summary>
        public float XVelMin { get; set; }
        /// <summary>
        /// Gets or Sets the Maximum X Velocity
        /// </summary>
        public float XVelMax { get; set; }
        /// <summary>
        /// Gets or Sets the Y Minimum Velocity
        /// </summary>
        public float YVelMin { get; set; }
        /// <summary>
        /// Gets or Sets the Maximum Y Velocity
        /// </summary>
        public float YVelMax { get; set; }
        /// <summary>
        /// Gets or Sets the Accelertion on the X axis
        /// </summary>
        public float AccelarationX { get; set; }
        /// <summary>
        /// Gets or Sets the Acceleration on the Y axis
        /// </summary>
        public float AccelarationY { get; set; }
        /// <summary>
        /// Gets or Sets the Position of The Creep
        /// </summary>
        public RectangleXNA Position { get; set; }
        /// <summary>
        /// Gets or Sets the Percentage of the Creeps Bounding Box on the X Axis
        /// </summary>
        public float BBoxXpercent { get; set; }
        /// <summary>
        /// Gets or Sets the Percentage of the Creeps Bounding Box on the Y Axis
        /// </summary>
        public float BBoxYpercent { get; set; }
        /// <summary>
        /// Gets or Sets the integer of the Explode Animation
        /// </summary>
        public int ExplodeAnimation { get; set; }
        /// <summary>
        /// Gets or Sets the Explode Animation Size
        /// </summary>
        public int ExplodeAnimationSize { get; set; }
        /// <summary>
        /// Gets or Sets the Health Bar Offset Of the Creep
        /// </summary>
        public Vextor2XNA HealthBarOffset { get; set; }
        /// <summary>
        /// Gets or Sets The Seconds Between Events (Minimum)
        /// </summary>
        public int SecondsBetweenEventsMin { get; set; }
        /// <summary>
        /// Gets or Sets the Seconds Between Events (Maximum)
        /// </summary>
        public int SecondsBetweenEventsMax { get; set; }
        /// <summary>
        /// Gets or Sets the Minimum Number of Creeps to Make
        /// </summary>
        public int NumberToMakeMin { get; set; }
        /// <summary>
        /// Gets or Sets the Maximum Number of Creeps to Make
        /// </summary>
        public int NumberToMakeMax { get; set; }
        /// <summary>
        /// Gets or Sets the Total Number of Creeps to Make
        /// </summary>
        public int TotalNumberToMake { get; set; }
        /// <summary>
        /// Gets or Sets the BombId for the Creep
        /// </summary>
        public int BombId { get; set; }
        /// <summary>
        /// Gets or Sets the Seconds Between Making A Creep (Minimum)
        /// </summary>
        public int Bomb_SecondsBetweenMakeEventsMin { get; set; }
        /// <summary>
        /// Gets or Sets the Seconds Between Making A Creep (Maximum)
        /// </summary>
        public int Bomb_SecondsBetweenMakeEventsMax { get; set; }
        /// <summary>
        /// Gets or Sets the Bomb0 Angle
        /// </summary>
        public float Bomb0_Angle { get; set; }
        /// <summary>
        /// Gets or Sets the Bomb1 Angle
        /// </summary>
        public float Bomb1_Angle { get; set; }
        /// <summary>
        /// Gets or Sets the Bomb2 Angle
        /// </summary>
        public float Bomb2_Angle { get; set; }
        /// <summary>
        /// Gets or Sets the Bomb0 Speed
        /// </summary>
        public float Bomb_Speed0 { get; set; }
        /// <summary>
        /// Gets or Sets the Bomb1 Speed
        /// </summary
        public float Bomb_Speed1 { get; set; }
        /// <summary>
        /// Gets or Sets the Bomb2 Speed
        /// </summary
        public float Bomb_Speed2 { get; set; }

        #endregion

    }
}
