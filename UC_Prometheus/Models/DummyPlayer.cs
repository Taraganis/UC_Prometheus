﻿///Basic Code Generating Class Model For the UC Game Generator //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Dummy Player Model for the UI

namespace UC_Prometheus.Models
{
    public class DummyPlayer : Player
    {

        public DummyPlayer()
        {
            this.PlayerImage = @"Players\Triangle0";
            this.MaxAmmo = 3;
            this.RightVelocity = 2;
            this.LeftVelocity = -2;
            this.UpVelocity = -2;
            this.DownVelocity = 2;
            this.HitPoints = 5;
            this.Time = 50;
            this.BBoxXpercent = 100;
            this.BBoxYpercent = 100;
            this.Height = 64;
            this.Width = -1;
            this.ExplodeAnimation = 0;
            this.ExplodeAnimationSize = 128;
            this.BulletId = 1;
            this.Time = 60;
            this.MaxAmmo = 3;
            RectangleXNA rec;
            rec.x = 200;
            rec.y = 500;
            rec.width = 400;
            rec.height = 100;

            this.PlayerZone = rec;
        }
    }
}
