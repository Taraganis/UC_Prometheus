﻿///Basic Code Generating Class Model For the UC Game Generator //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UC_Prometheus;
using UC_Prometheus.Models;
using UC_Prometheus.Code_Generation;

namespace UC_Prometheus.Models
{
    public enum FrameWork
    {
        XNA, Monogame
    };
    public class FullGame
    {
        #region Properties
        
        /// <summary>
        /// The Name of the game to be produced
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The screens for the game to be produced
        /// </summary>
        public Screen[] screens { get; set; }
        /// <summary>
        /// The levels for the game to be produced
        /// </summary>
        public Level[] levels { get; set; }
        /// <summary>
        /// The bullets for the game to be produced
        /// </summary>
        public List<Bullet> bullets { get; set; }
        /// <summary>
        /// The creeps for the game to be produced
        /// </summary>
        public Creep[] creeps { get; set; }
        /// <summary>
        /// Gets or Sets the Screen Width (Resolution)
        /// </summary>
        public int ScreenWidth { get; set; }
        /// <summary>
        /// Gets or Sets the Screen Height (Resolution)
        /// </summary>
        public int ScreenHeight { get; set; }
        /// <summary>
        /// The FrameWork in which the game is beign build
        /// </summary>
        public FrameWork framework { get; set; }

        #endregion

        /// <summary>
        /// Parameterless Constructor. Sets the game by default to XNA
        /// </summary>
        public FullGame()
        {
            this.framework = FrameWork.XNA;
        }
    }
}
