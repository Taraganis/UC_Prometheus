﻿///Basic Code Generating Class Model For the UC Game Generator //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Dummy Bullets for the UI

namespace UC_Prometheus.Models
{

    /// <summary>
    /// A Dummy Creep Designed for the UI
    /// </summary>
    public class DummyBulletCreep : Bullet
    {

        /// <summary>
        /// Initiates A Dummy Bullet for the UI
        /// </summary>
        public DummyBulletCreep()
        {

            this.Id = 0;
            this.Damage = 10;
            this.Height = -1;
            this.Width = 8;
            this.Image = @"CreepBombs\red1";
            this.Name = "red1";
            this.XAccelaration = 0.01f;
            this.YAccelaration = 0.01f;
            this.XVelocity = 0;
            this.YVelocity = 2;
            this.BBoxXpercent = 100;
            this.BBoxYpercent = 100;
            this.ExplodeAnimation = 0;
            this.ExplodeAnimationSize = 128;
            this.Radius = 0;
        }

    }
    /// <summary>
    /// A Dummy Player Designed for the UI
    /// </summary>
    public class DummyBulletPlayer : Bullet
    {

        /// <summary>
        /// Initiates A Dummy Player for the UI
        /// </summary>
        public DummyBulletPlayer()
        {

            this.Id = 1;
            this.Damage = 6;
            this.Height = -1;
            this.Width = 8;
            this.Image = @"CreepBombs\down";
            this.Name = "Shot";
            this.XAccelaration = 0.01f;
            this.YAccelaration = 0.01f;
            this.XVelocity = 0;
            this.YVelocity = -3;
            this.BBoxXpercent = 100;
            this.BBoxYpercent = 100;
            this.ExplodeAnimation = 0;
            this.ExplodeAnimationSize = 128;
            this.Radius = 40;
        }

    }
}
