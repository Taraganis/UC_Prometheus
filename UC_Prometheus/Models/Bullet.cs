﻿///Basic Code Generating Class Model For the UC Game Generator //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UC_Prometheus.Models
{
    public class Bullet
    {
        #region Properties

        /// <summary>
        /// the Id of the Bullet
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// The Image path of the bullet
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// The name of the bullet
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Gets or Sets the Width of teh Bullet
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// Gets or Sets the Height of the Bullet
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// Gets or Sets the HitPonts of the Bullet
        /// </summary>
        public int HitPoints { get; set; }
        /// <summary>
        /// Gets or Sets the X Velocitty of the Bullet
        /// </summary>
        public float XVelocity { get; set; }
        /// <summary>
        /// Gets or Sets the Y Velocitty of the Bullet
        /// </summary>
        public float YVelocity { get; set; }
        /// <summary>
        /// Gets oor Sets the X Acceleration of the Bullet
        /// </summary>
        public float XAccelaration { get; set; }
        /// <summary>
        /// Gets or Sets the Y Acceleration of the Bullet
        /// </summary>
        public float YAccelaration { get; set; }
        /// <summary>
        /// Gets r Sets the Damage of the Bullet
        /// </summary>
        public float Damage { get; set; }
        /// <summary>
        /// Gets or Sets the Percentage of the X Axis of the Bounding Box of the Bullet
        /// </summary>
        public float BBoxXpercent { get; set; }
        /// <summary>
        /// Gets or Sets the Percentage of the Y Axis of the Bounding Box of the Bullet
        /// </summary>
        public float BBoxYpercent { get; set; }
        /// <summary>
        /// Gets or Sets the Radius of the Explosion
        /// </summary>
        public float Radius { get; set; }
        /// <summary>
        /// Gets or sets the Integer of the Explode Animation
        /// </summary>
        public int ExplodeAnimation { get; set; }
        /// <summary>
        /// Gets or Sets the Size of the Explotion
        /// </summary>
        public int ExplodeAnimationSize { get; set; }

        #endregion

    }
}
