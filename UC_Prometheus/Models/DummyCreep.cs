﻿///Basic Code Generating Class Model For the UC Game Generator //
/// Written by Michail Taraganis                              //
/// Suppervised by Robert Cox                                //
/// Copyright 2015                                          //
/////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Dummy Creep Model for the UI

namespace UC_Prometheus.Models
{
    /// <summary>
    /// A Dummy Creep Designed for the UI
    /// </summary>
    public class DummyCreep : Creep
    {
        /// <summary>
        /// Initiates a Dummy Creep for the UI
        /// </summary>
        public DummyCreep()
        {
            this.Id = 0;
            this.Image = "AtkButterfly";
            this.Name = "Creep1";
            this.Width = 60;
            this.Height = -1;
            RectangleXNA rec;
            rec.x = 200;
            rec.y = -30;
            rec.width = 200;
            rec.height = 20;
            this.Position = rec;
            this.BBoxXpercent = 100;
            this.BBoxYpercent = 100;
            this.XVelMin = 0;
            this.XVelMax = 1;
            this.YVelMax = 3;
            this.YVelMin = 1;
            this.AccelarationY = 0.01f;
            this.AccelarationX = 0.01f;
            this.HealthBar = true;
            Vextor2XNA vec;
            vec.x = -20;
            vec.y = -10;
            this.HealthBarOffset = vec;
            this.SecondTillActive = 15;
            this.SecondsToBeActive = 0;
            this.WarningMessage = "";
            this.SecondsBetweenEventsMin = 3;
            this.SecondsBetweenEventsMax = 3;
            this.NumberToMakeMin = 1;
            this.NumberToMakeMax = 1;
            this.TotalNumberToMake = 15;
            this.HitPoints = 1;
            this.Score = 10;
            this.BombId = 0;
            this.Bomb_SecondsBetweenMakeEventsMin = 2;
            this.Bomb_SecondsBetweenMakeEventsMax = 2;
            this.Bomb0_Angle = 90;
            this.Bomb1_Angle = 85;
            this.Bomb2_Angle = 95;
            this.Bomb_Speed0 = 3f;
            this.Bomb_Speed1 = 2.9f;
            this.Bomb_Speed2 = 2.9f;
        }
    }
}
