﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UC_Prometheus;
using UC_Prometheus.Code_Generation;
using UC_Prometheus.Models;

namespace Class_Generation_Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //LevelTest();
            Console.WriteLine("L for Level, \nB for Bullet, \nC for Creep, \nP for Parameters, \nF for Full Game" );

            string key = Console.ReadLine();

            switch(key)
            {
                case "l":
                    LevelTest();
                    break;
                case "b":
                    BulletTest();
                    break;
                case "c":
                    CreepTest();
                    break;
                case "f":
                    FullGameTest();
                    break;
                case "p":
                    ParameterTest();
                    break;
                case "g":
                    Game1Test();
                    break;
                default:
                    Console.WriteLine("Order not found");
                    break;
            }

            Console.ReadLine();

        }

        private static void Game1Test()
        {
            try
            {
                DataBank.GameDirectory = Directory.GetCurrentDirectory() + "\\Output\\";
                DataBank.ProjectName = "Testing123";

                DirectoryUtilities.CopyClasses(DataBank.GameDirectory);

                Console.WriteLine("Game1 Class Generated Successfully");
            }
            catch(Exception e)
            {
                Console.WriteLine("Unable to Create Class");
            }
        }

        private static void LevelTest()
        {
            Level testLevel = new Level();
            testLevel.Name = "Test";
            testLevel.Id = 0;
            testLevel.ScrollSpeed = 1;
            testLevel.PlayerZone = "new Vector2(200,200)";
            testLevel.showExplosion = false;
            testLevel.Background = @"Backgrounds\Sky16";
           
            RectangleXNA rect;
            rect.x = 0;
            rect.y = 0;
            rect.width = 800;
            rect.height = 600;
            testLevel.ScreenArea = rect;
             


            Player player = new Player();
            player.PlayerImage = @"Players\Triangle0";
            player.MaxAmmo = 3;
            player.RightVelocity = 2;
            player.LeftVelocity = -2;
            player.UpVelocity = -2;
            player.DownVelocity = 2;
            player.HitPoints = 5;
            player.Time = 50;
            player.BBoxXpercent = 100;
            player.BBoxYpercent = 100;
            player.Height = 64;
            player.Width = -1;
            player.ExplodeAnimation = 0;
            player.ExplodeAnimationSize = 128;
            player.BulletId = 1;
            player.Time = 60;
            player.MaxAmmo = 3;

            RectangleXNA rec;
            rec.x = 200;
            rec.y = 500;
            rec.width = 400;
            rec.height = 100;

            player.PlayerZone = rec;

            testLevel.player = player;

            DataBank.GameDirectory = Directory.GetCurrentDirectory() + "\\Output";

            Level_Generator lg = new Level_Generator();
            lg.CreateClass(testLevel);

            Console.WriteLine("Class Generated");

        }


        private static void CreepTest()
        {
            Creep creep = new Creep();

            creep.Id = 0;
            creep.Image = "AtkButterfly";
            creep.Name = "Creep1";
            creep.Width = 60;
            creep.Height = -1;
            RectangleXNA rec;
            rec.x = 200;
            rec.y = -30;
            rec.width = 200;
            rec.height = 20;
            creep.Position = rec;
            creep.BBoxXpercent = 100;
            creep.BBoxYpercent = 100;
            creep.XVelMin = 0;
            creep.XVelMax = 1;
            creep.YVelMax = 3;
            creep.YVelMin = 1;
            creep.AccelarationY = 0.01f;
            creep.AccelarationX = 0.01f;
            creep.HealthBar = true;
            Vextor2XNA vec;
            vec.x = -20;
            vec.y = -10;
            creep.HealthBarOffset = vec;
            creep.SecondTillActive = 15;
            creep.SecondsToBeActive = 0;
            creep.WarningMessage = "";
            creep.SecondsBetweenEventsMin = 3;
            creep.SecondsBetweenEventsMax = 3;
            creep.NumberToMakeMin = 1;
            creep.NumberToMakeMax = 1;
            creep.TotalNumberToMake = 15;
            creep.HitPoints = 1;
            creep.Score = 10;
            creep.BombId = 0;
            creep.Bomb_SecondsBetweenMakeEventsMin = 2;
            creep.Bomb_SecondsBetweenMakeEventsMax = 2;
            creep.Bomb0_Angle = 90;
            creep.Bomb1_Angle = 85;
            creep.Bomb2_Angle = 95;
            creep.Bomb_Speed0 = 3f;
            creep.Bomb_Speed1 = 2.9f;
            creep.Bomb_Speed2 = 2.9f;

            Creep_Generator cg = new Creep_Generator();
            cg.CreateClass(creep);

            creep.Id = 1;
            creep.Image = "Square";
            creep.Name = "Creep2";
            creep.Width = 40;
            creep.Height = -1;
            rec.x = 200;
            rec.y = -30;
            rec.width = 400;
            rec.height = 20;
            creep.Position = rec;
            creep.BBoxXpercent = 100;
            creep.BBoxYpercent = 100;
            creep.XVelMin = 0;
            creep.XVelMax = 0;
            creep.YVelMax = 2;
            creep.YVelMin = 2;
            creep.AccelarationY = 0.01f;
            creep.AccelarationX = 0.01f;
            creep.HealthBar = false;       
            vec.x = 0;
            vec.y = -10;
            creep.HealthBarOffset = vec;
            creep.SecondTillActive = 5;
            creep.SecondsToBeActive = 0;
            creep.WarningMessage = "";
            creep.SecondsBetweenEventsMin = 3;
            creep.SecondsBetweenEventsMax = 3;
            creep.NumberToMakeMin = 1;
            creep.NumberToMakeMax = 2;
            creep.TotalNumberToMake = 15;
            creep.HitPoints = 10;
            creep.Score = 20;
            creep.BombId = -1;
            creep.Bomb_SecondsBetweenMakeEventsMin = 3;
            creep.Bomb_SecondsBetweenMakeEventsMax = 3;
            creep.Bomb0_Angle = 90;
            creep.Bomb1_Angle = 85;
            creep.Bomb2_Angle = 95;
            creep.Bomb_Speed0 = 3f;
            creep.Bomb_Speed1 = 3f;
            creep.Bomb_Speed2 = 3f;

            cg.CreateClass(creep);

            Console.WriteLine("Generation Complete");
        }

        private static void PlayerTest()
        {
            Player player = new Player();
            player.PlayerImage = @"Players\Triangle0";
            player.MaxAmmo = 3;
            player.RightVelocity = 2;
            player.LeftVelocity = -2;
            player.UpVelocity = -2;
            player.DownVelocity = 2;
            player.HitPoints = 5;
            player.Time = 50;

        }

        private static void ScreensTest()
        {

        }

        private static void GameParametesTest()
        {

        }

        private static void BulletTest()
        {
            Bullet bullet = new Bullet();
            bullet.Id = 0;
            bullet.Damage = 10;
            bullet.Height = -1;
            bullet.Width = 8;
            bullet.Image = @"CreepBombs\red1";
            bullet.Name = "red1";
            bullet.XAccelaration = 0.01f;
            bullet.YAccelaration = 0.01f;
            bullet.XVelocity = 0;
            bullet.YVelocity = 2;
            bullet.BBoxXpercent = 100;
            bullet.BBoxYpercent = 100;
            bullet.ExplodeAnimation = 0;
            bullet.ExplodeAnimationSize = 128;
            bullet.Radius = 0;

            Bullet_Generator bg = new Bullet_Generator();

            bg.CreateClass(bullet);

            Bullet bullet2 = new Bullet();
            bullet2.Id = 1;
            bullet2.Damage = 6;
            bullet2.Height = -1;
            bullet2.Width = 8;
            bullet2.Image = @"CreepBombs\down";
            bullet2.Name = "Shot";
            bullet2.XAccelaration = 0.01f;
            bullet2.YAccelaration = 0.01f;
            bullet2.XVelocity = 0;
            bullet2.YVelocity = -3;
            bullet2.BBoxXpercent = 100;
            bullet2.BBoxYpercent = 100;
            bullet2.ExplodeAnimation = 0;
            bullet2.ExplodeAnimationSize = 128;
            bullet2.Radius = 40;

            bg.CreateClass(bullet2);

            Console.WriteLine("Generation Complete");
        }

        private static void ParameterTest()
        {
            DataBank.fullGame = new FullGame();


            #region Create and Store Bullets

            Bullet bullet = new Bullet();
            bullet.Id = 0;
            bullet.Damage = 10;
            bullet.Height = -1;
            bullet.Width = 8;
            bullet.Image = @"CreepBombs\red1";
            bullet.Name = "red1";
            bullet.XAccelaration = 0.01f;
            bullet.YAccelaration = 0.01f;
            bullet.XVelocity = 0;
            bullet.YVelocity = 2;
            bullet.BBoxXpercent = 100;
            bullet.BBoxYpercent = 100;
            bullet.ExplodeAnimation = 0;
            bullet.ExplodeAnimationSize = 128;
            bullet.Radius = 0;

            DataBank.fullGame.bullets = new List<Bullet>();
            DataBank.fullGame.bullets.Add(bullet);

            Bullet bullet2 = new Bullet();
            bullet2.Id = 1;
            bullet2.Damage = 6;
            bullet2.Height = -1;
            bullet2.Width = 8;
            bullet2.Image = @"CreepBombs\down";
            bullet2.Name = "Shot";
            bullet2.XAccelaration = 0.01f;
            bullet2.YAccelaration = 0.01f;
            bullet2.XVelocity = 0;
            bullet2.YVelocity = -3;
            bullet2.BBoxXpercent = 100;
            bullet2.BBoxYpercent = 100;
            bullet2.ExplodeAnimation = 0;
            bullet2.ExplodeAnimationSize = 128;
            bullet2.Radius = 40;

            DataBank.fullGame.bullets.Add(bullet2);

            Console.WriteLine("Bullets stored in memory");

            #endregion

            #region Create And Store Creeps

            Creep creep = new Creep();

            creep.Id = 0;
            creep.Image = "AtkButterfly";
            creep.Name = "Creep1";
            creep.Width = 60;
            creep.Height = -1;
            RectangleXNA rec;
            rec.x = 200;
            rec.y = -30;
            rec.width = 200;
            rec.height = 20;
            creep.Position = rec;
            creep.BBoxXpercent = 100;
            creep.BBoxYpercent = 100;
            creep.XVelMin = 0;
            creep.XVelMax = 1;
            creep.YVelMax = 3;
            creep.YVelMin = 1;
            creep.AccelarationY = 0.01f;
            creep.AccelarationX = 0.01f;
            creep.HealthBar = true;
            Vextor2XNA vec;
            vec.x = -20;
            vec.y = -10;
            creep.HealthBarOffset = vec;
            creep.SecondTillActive = 15;
            creep.SecondsToBeActive = 0;
            creep.WarningMessage = "";
            creep.SecondsBetweenEventsMin = 3;
            creep.SecondsBetweenEventsMax = 3;
            creep.NumberToMakeMin = 1;
            creep.NumberToMakeMax = 1;
            creep.TotalNumberToMake = 15;
            creep.HitPoints = 1;
            creep.Score = 10;
            creep.BombId = 0;
            creep.Bomb_SecondsBetweenMakeEventsMin = 2;
            creep.Bomb_SecondsBetweenMakeEventsMax = 2;
            creep.Bomb0_Angle = 90;
            creep.Bomb1_Angle = 85;
            creep.Bomb2_Angle = 95;
            creep.Bomb_Speed0 = 3f;
            creep.Bomb_Speed1 = 2.9f;
            creep.Bomb_Speed2 = 2.9f;

            DataBank.fullGame.creeps = new Creep[2];
            DataBank.fullGame.creeps[0] = creep;

            Creep creepx = new Creep();

            creepx.Id = 1;
            creepx.Image = "Square";
            creepx.Name = "Creep2";
            creepx.Width = 40;
            creepx.Height = -1;
            rec.x = 200;
            rec.y = -30;
            rec.width = 400;
            rec.height = 20;
            creepx.Position = rec;
            creepx.BBoxXpercent = 100;
            creepx.BBoxYpercent = 100;
            creepx.XVelMin = 0;
            creepx.XVelMax = 0;
            creepx.YVelMax = 2;
            creepx.YVelMin = 2;
            creepx.AccelarationY = 0.01f;
            creepx.AccelarationX = 0.01f;
            creepx.HealthBar = false;
            vec.x = 0;
            vec.y = -10;
            creepx.HealthBarOffset = vec;
            creepx.SecondTillActive = 5;
            creepx.SecondsToBeActive = 0;
            creepx.WarningMessage = "";
            creepx.SecondsBetweenEventsMin = 3;
            creepx.SecondsBetweenEventsMax = 3;
            creepx.NumberToMakeMin = 1;
            creepx.NumberToMakeMax = 2;
            creepx.TotalNumberToMake = 15;
            creepx.HitPoints = 10;
            creepx.Score = 20;
            creepx.BombId = -1;
            creepx.Bomb_SecondsBetweenMakeEventsMin = 3;
            creepx.Bomb_SecondsBetweenMakeEventsMax = 3;
            creepx.Bomb0_Angle = 90;
            creepx.Bomb1_Angle = 85;
            creepx.Bomb2_Angle = 95;
            creepx.Bomb_Speed0 = 3f;
            creepx.Bomb_Speed1 = 3f;
            creepx.Bomb_Speed2 = 3f;

            DataBank.fullGame.creeps[1] = creepx;

            Console.WriteLine("Creeps stored in memory");

            #endregion

            #region Create And Store Levels

            Level testLevel = new Level();
            testLevel.Name = "Test";
            testLevel.Id = 0;
            testLevel.ScrollSpeed = 1;
            testLevel.PlayerZone = "new Vector2(200,200)";
            testLevel.showExplosion = false;
            testLevel.Background = @"Backgrounds\Sky16";
      
            RectangleXNA rect;
            rect.x = 0;
            rect.y = 0;
            rect.width = 800;
            rect.height = 600;
            testLevel.ScreenArea = rect;
            Creep creep1 = new Creep();
            creep1.Name = "Creep1";

            Creep creep2 = new Creep();
            creep2.Name = "Creep1";




            Player player = new Player();
            player.PlayerImage = @"Players\Triangle0";
            player.MaxAmmo = 3;
            player.RightVelocity = 2;
            player.LeftVelocity = -2;
            player.UpVelocity = -2;
            player.DownVelocity = 2;
            player.HitPoints = 5;
            player.Time = 50;
            player.BBoxXpercent = 100;
            player.BBoxYpercent = 100;
            player.Height = 64;
            player.Width = -1;
            player.ExplodeAnimation = 0;
            player.ExplodeAnimationSize = 128;
            player.BulletId = 1;
            player.Time = 60;
            player.MaxAmmo = 3;


            rec.x = 200;
            rec.y = 500;
            rec.width = 400;
            rec.height = 100;

            player.PlayerZone = rec;

            testLevel.player = player;

            DataBank.fullGame.levels = new Level[1];
            DataBank.fullGame.levels[0] = testLevel;

            Console.WriteLine("Levels stored in memory");

            #endregion

            try
            {
                ParameterClass_Gereration pg = new ParameterClass_Gereration();
                pg.CreateClass(DataBank.fullGame);  //Generating the Parameter Class
                Console.WriteLine("Parameter Class Generation Complete");
            }
            catch (Exception e)
            {
                Console.WriteLine("The Following Error Has Occured:");
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }
        }

        private static void FullGameTest()
        {
            DataBank.fullGame = new FullGame();


            DataBank.fullGame.ScreenWidth = 640;
            DataBank.fullGame.ScreenHeight = 480;

            #region Create and Store Bullets

            Bullet bullet = new Bullet();
            bullet.Id = 0;
            bullet.Damage = 10;
            bullet.Height = -1;
            bullet.Width = 8;
            bullet.Image = @"CreepBombs\red1";
            bullet.Name = "red1";
            bullet.XAccelaration = 0.01f;
            bullet.YAccelaration = 0.01f;
            bullet.XVelocity = 0;
            bullet.YVelocity = 2;
            bullet.BBoxXpercent = 100;
            bullet.BBoxYpercent = 100;
            bullet.ExplodeAnimation = 0;
            bullet.ExplodeAnimationSize = 128;
            bullet.Radius = 0;

            DataBank.fullGame.bullets = new List<Bullet>();
            DataBank.fullGame.bullets.Add(bullet);

            Bullet bullet2 = new Bullet();
            bullet2.Id = 1;
            bullet2.Damage = 6;
            bullet2.Height = -1;
            bullet2.Width = 8;
            bullet2.Image = @"CreepBombs\down";
            bullet2.Name = "Shot";
            bullet2.XAccelaration = 0.01f;
            bullet2.YAccelaration = 0.01f;
            bullet2.XVelocity = 0;
            bullet2.YVelocity = -3;
            bullet2.BBoxXpercent = 100;
            bullet2.BBoxYpercent = 100;
            bullet2.ExplodeAnimation = 0;
            bullet2.ExplodeAnimationSize = 128;
            bullet2.Radius = 40;

            DataBank.fullGame.bullets.Add(bullet2);

            Console.WriteLine("Bullets stored in memory");

            #endregion

            #region Create And Store Creeps

            Creep creep = new Creep();

            creep.Id = 0;
            creep.Image = "AtkButterfly";
            creep.Name = "Creep1";
            creep.Width = 60;
            creep.Height = -1;
            RectangleXNA rec;
            rec.x = 200;
            rec.y = -30;
            rec.width = 200;
            rec.height = 20;
            creep.Position = rec;
            creep.BBoxXpercent = 100;
            creep.BBoxYpercent = 100;
            creep.XVelMin = 0;
            creep.XVelMax = 1;
            creep.YVelMax = 3;
            creep.YVelMin = 1;
            creep.AccelarationY = 0.01f;
            creep.AccelarationX = 0.01f;
            creep.HealthBar = true;
            Vextor2XNA vec;
            vec.x = -20;
            vec.y = -10;
            creep.HealthBarOffset = vec;
            creep.SecondTillActive = 15;
            creep.SecondsToBeActive = 0;
            creep.WarningMessage = "";
            creep.SecondsBetweenEventsMin = 3;
            creep.SecondsBetweenEventsMax = 3;
            creep.NumberToMakeMin = 1;
            creep.NumberToMakeMax = 1;
            creep.TotalNumberToMake = 15;
            creep.HitPoints = 1;
            creep.Score = 10;
            creep.BombId = 0;
            creep.Bomb_SecondsBetweenMakeEventsMin = 2;
            creep.Bomb_SecondsBetweenMakeEventsMax = 2;
            creep.Bomb0_Angle = 90;
            creep.Bomb1_Angle = 85;
            creep.Bomb2_Angle = 95;
            creep.Bomb_Speed0 = 3f;
            creep.Bomb_Speed1 = 2.9f;
            creep.Bomb_Speed2 = 2.9f;

            DataBank.fullGame.creeps = new Creep[2];
            DataBank.fullGame.creeps[0] = creep;

            Creep creepx = new Creep();

            creepx.Id = 1;
            creepx.Image = "Square";
            creepx.Name = "Creep2";
            creepx.Width = 40;
            creepx.Height = -1;
            rec.x = 200;
            rec.y = -30;
            rec.width = 400;
            rec.height = 20;
            creepx.Position = rec;
            creepx.BBoxXpercent = 100;
            creepx.BBoxYpercent = 100;
            creepx.XVelMin = 0;
            creepx.XVelMax = 0;
            creepx.YVelMax = 2;
            creepx.YVelMin = 2;
            creepx.AccelarationY = 0.01f;
            creepx.AccelarationX = 0.01f;
            creepx.HealthBar = false;
            vec.x = 0;
            vec.y = -10;
            creepx.HealthBarOffset = vec;
            creepx.SecondTillActive = 5;
            creepx.SecondsToBeActive = 0;
            creepx.WarningMessage = "";
            creepx.SecondsBetweenEventsMin = 3;
            creepx.SecondsBetweenEventsMax = 3;
            creepx.NumberToMakeMin = 1;
            creepx.NumberToMakeMax = 2;
            creepx.TotalNumberToMake = 15;
            creepx.HitPoints = 10;
            creepx.Score = 20;
            creepx.BombId = -1;
            creepx.Bomb_SecondsBetweenMakeEventsMin = 3;
            creepx.Bomb_SecondsBetweenMakeEventsMax = 3;
            creepx.Bomb0_Angle = 90;
            creepx.Bomb1_Angle = 85;
            creepx.Bomb2_Angle = 95;
            creepx.Bomb_Speed0 = 3f;
            creepx.Bomb_Speed1 = 3f;
            creepx.Bomb_Speed2 = 3f;

            DataBank.fullGame.creeps[1] = creepx;

            Console.WriteLine("Creeps stored in memory");

            #endregion

            #region Create And Store Levels

            Level testLevel = new Level();
            testLevel.Name = "Test";
            testLevel.Id = 0;
            testLevel.ScrollSpeed = 1;
            testLevel.PlayerZone = "new Vector2(200,200)";
            testLevel.showExplosion = false;
            testLevel.Background = @"Backgrounds\Sky16";
        

            RectangleXNA rect;
            rect.x = 0;
            rect.y = 0;
            rect.width = 800;
            rect.height = 600;
            testLevel.ScreenArea = rect;
            Creep creep1 = new Creep();
            creep1.Name = "Creep1";

            Creep creep2 = new Creep();
            creep2.Name = "Creep1";

      



            Player player = new Player();
            player.PlayerImage = @"Players\Triangle0";
            player.MaxAmmo = 3;
            player.RightVelocity = 2;
            player.LeftVelocity = -2;
            player.UpVelocity = -2;
            player.DownVelocity = 2;
            player.HitPoints = 5;
            player.Time = 50;
            player.BBoxXpercent = 100;
            player.BBoxYpercent = 100;
            player.Height = 64;
            player.Width = -1;
            player.ExplodeAnimation = 0;
            player.ExplodeAnimationSize = 128;
            player.BulletId = 1;
            player.Time = 60;
            player.MaxAmmo = 3;

           
            rec.x = 200;
            rec.y = 500;
            rec.width = 400;
            rec.height = 100;

            player.PlayerZone = rec;

            testLevel.player = player;

            DataBank.fullGame.levels = new Level[1];
            DataBank.fullGame.levels[0] = testLevel;

            Console.WriteLine("Levels stored in memory");

            #endregion

            #region Create Screens


            Screen intro = new Screen();
            intro.Name = "Intro";
            intro.BigText = "Michael and Rhi Did it!";
            intro.MediumText = "By Michail and Rhi";
            intro.MouseImage = "Mouse7";
            intro.ButtonImage = "Oval";
            intro.Image = "Sky2";
            intro.Border = "Border9";
            intro.MediumText = "";
            intro.MediumText2 = "";
            intro.MediumText3 = "";
            intro.ContinueText = "Click here";

            DataBank.fullGame.screens = new Screen[4];
            DataBank.fullGame.screens[0] = intro;

            Screen win = new Screen();
            win.Name = "Win";
            win.BigText = "Michael and Rhi Did it!";
            win.MediumText = "By Michail and Rhi";
            win.MouseImage = "Mouse7";
            win.ButtonImage = "Oval";
            win.Image = "Sky9";
            win.Border = "Border9";
            win.MediumText = "";
            win.MediumText2 = "";
            win.MediumText3 = "";
            win.ContinueText = "Click here";

            DataBank.fullGame.screens[1] = win;

            Screen fail = new Screen();
            fail.Name = "Fail";
            fail.BigText = "Michael and Rhi Did it!";
            fail.MediumText = "By Michail and Rhi";
            fail.MouseImage = "Mouse7";
            fail.ButtonImage = "Oval";
            fail.Image = "Sky2";
            fail.Border = "Border9";
            fail.MediumText = "";
            fail.MediumText2 = "";
            fail.MediumText3 = "";
            fail.ContinueText = "Click here";

            DataBank.fullGame.screens[2] = fail;

            Screen menu = new Screen();
            menu.Name = "Menu";
            menu.BigText = "Michael and Rhi Did it!";
            menu.MediumText = "By Michail and Rhi";
            menu.MouseImage = "Mouse7";
            menu.ButtonImage = "Oval";
            menu.Image = "Sky7";
            menu.Border = "Border9";
            menu.MediumText = "";
            menu.MediumText2 = "";
            menu.MediumText3 = "";
            menu.ContinueText = "Click here";

            DataBank.fullGame.screens[3] = menu;

            #endregion

            try
            {
                DirectoryUtilities.CreateDirectory(DataBank.GameName);
                DirectoryUtilities.CreateGame();
            }
            catch(Exception e)
            {
                Console.WriteLine("The Following Error Has Occured:");
                Console.WriteLine(e.StackTrace);
                Console.ReadLine();
            }
            

        }
    }
}
